meteor build ../
cd ..
tar -zxvf nge.tar.gz
cd bundle
## npm dependencies
npm install fibers
npm install source-map-support
npm install underscore
npm install semver
## env variables
#export MONGO_URL='mongodb://localhost:27017/nge'
#export PORT=3001
#export ROOT_URL="http://nge.buzzbizz.biz"
## pm2
##pm2 stop "Northwestern Geotech"
##pm2 start main.js --name "Northwestern Geotech"
pm2 delete 'Nge'
DISABLE_WEBSOCKETS=1 MONGO_URL='mongodb://localhost:27017/nge' PORT=3001 ROOT_URL='http://nge.buzzbizz.biz' pm2 start main.js --name 'Nge' -f