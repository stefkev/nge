
Meteor.startup(function() {
    if (!Meteor.users.findOne({
        username: 'admin'
    })) {
        var u = Accounts.createUser({
            username: 'admin',
            email: 'web@bbsalaska.com',
            password: 'webAdmin120-2'
        });
        // this just doesn't work at startup
        if (u) {
            if (!Roles.addUserToRoles(u, 'admin')) {
                throw 'Could not set role for admin';
            }
        } else {
            throw 'Could not create admin user';
        }
    } else {

        console.log('admin inserted already');
    }

    if (!Meteor.users.findOne({
        username: 'testClient'
    })) {
        var u = Accounts.createUser({
            username: 'testClient',
            email: 'web2@bbsalaska.com',
            password: 'webAdmin120-4'
        });
    }

});
if(typeof FS != "undefined"){
    Meteor.publish("documents", function() {
        if (this.userId && Roles.userHasRole(this.userId, 'admin')) {
            return Documents.find();
        } else if(this.userId) {
            return Documents.find({owner:this.userId});
        }
        return false;
    });
}
Meteor.publish("users", function() {
    if (this.userId && Roles.userHasRole(this.userId, 'admin')) {
        return Meteor.users.find();
    } else {
        return false;
    }
});
Contents = {
    'about': [{
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'The Northern Geotechnical Engineering- Terra Firma Testing Mission Statement',
        urlPath: '/about/company_bio'
    },{
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'The Northern Geotechnical Engineering- About:',
        urlPath: '/about'
        },
        {
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'The Northern Geotechnical Engineering- Terra Firma Testing Story:',
        urlPath: '/about/mission'
    }, {
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'Engineer and Engineering Technician Code of Ethics',
        urlPath: '/about/code_of_ethics'
    }, {
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'News and Upcoming Events',
        urlPath: '/about/news'
    }, {
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'Professional Affiliations',
        urlPath: '/about/pro_affiliations',
    }, {
        header: 'aboutHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        pageTitle: 'Client Endorsements',
        urlPath: '/about/client_reviews'
    }]
    ,
    
    'people': [{
        _layout : "peopleLayout",
        header: "peopleHeader",
        bodyBg: '/img/people_bg.svg',
        pageTitle: "Meet Our Team",
        urlPath: '/people',
        // /bio page doesn't exactly have a route... ? just make people/name ?
        subPages: [{
                urlPath : '/people/bio/kMobely',
                main: false,
                'pageTitle': 'Keith F. Mobley',

                'bioImage': 'bio_img_kmobley.jpg',
                'backLink': '/people',
                'content': '<p>Hello, my name is Keith Mobley and I am the President and Senior Geotechnical Engineer at NGE-TFT. I am licensed as a Professional Civil Engineer in the State of Alaska, as well as in California, where I am licensed as both a civil and geotechnical engineer. Since junior high school, I have known I would be an engineer. After graduating high school in Idaho Falls, Idaho, I earned degrees in Civil Engineering and Anthropology at Montana State University in 1976.</p><p>After undergraduate school, I lived in San Diego for four years working as a soils technician, laboratory manager, and junior civil engineer. During that time, I sat for the PE exam in California and passed. Although San Diego was nice, I missed the cold and snow, so I took a vacation to Alaska in February. In between skiing and climbing, I interviewed for a job in Anchorage and was offered a position before leaving to return to California. Two months later I was at my new home in Alaska.</p><p>Woodward-Clyde, Shannon & Wilson, and Golder Associates were all good to me, and for me, as I developed my engineering prowess. They all provided me with many exciting design and field projects all across Alaska (and one in Malaysia) that gave me tremendous experience. However, by the turn of the new millennium, I had decided that it was time to do my own thing. Over the years I developed core ideals regarding work quality, treating employees properly, and being properly rewarded for hard work. Northern Geotechnical Engineering was born in 2000 from these ideals. I met my current business partner, Ron Caron (owner and founder of Terra Firma Testing), in 2004, and after working together on several projects, we realized that we had similar business goals, and that our services could be mutually beneficial to both of our companies as well as our client base.  As a result, we merged Northern Geotechnical Engineering and Terra Firma Testing in the summer of 2004, forming NGE-TFT.</p><p>My professional activities outside of NGE-TFT include serving as the Vice Chair of the Geoprofessional Business Council within the Geoprofessional Business Association (GBA), and serving on the Municipality of Anchorage (MOA) Geotechnical Advisory Commission.</p><p>My passions outside of work include backpacking and dutch oven cooking, which I actively participate in through my involvement with the Boy Scouts of America.  I am an active member of the Order of the Arrow and an Eagle Scout (1968). I also enjoy traveling (both for work and pleasure).</p>'
            },
            {
                urlPath : '/people/bio/rCaron',
                main: false,
                'pageTitle': 'Ron J.P. Caron, C.E.T.',
                'bioImage': 'bio_img_rcaron.jpg',
                'backLink': '/people',
                'content': '<p>Greetings, I am Ron Caron, and I am a co-founder and Vice President of NGE-TFT.  I am registered as a Certified Engineering Technician by the Alberta Society of Engineering Technologists and I am responsible for the day-to-day operations of our materials testing and special inspection departments.  Some key words that define who I am are: integrity; commitment; and devotion to detail. One of my favorite sayings is “You only get one chance to make a first impression”, and I reinforce this mantra with all of our employees at NGE-TFT.   I use my professional experiences and foresight to proactively address issues before they become real problems, which has - in part, led to the success that our company has enjoyed over the years.</p><p>I grew up in Eastern Canada, and after graduating from Dawson College with an associate degree in civil engineering, I launched my professional career in Edmonton, Alberta; where I began my specialization in the field of advanced soil strength testing. Over the years I honed my technical skills in the laboratory environment (which is my true passion to date) and developed a mastery of my field, which eventually led to my first position in Alaska with EBA Engineering Consultants (now Tetra Tech). Upon arrival in Alaska, I quickly fell in love with all “The Last Frontier” had to offer and eventually learned how to hunt, fish, and prospect from the local Sourdoughs.  I founded Terra Firma Testing in Anchorage, Alaska in 1998 in an attempt to offer quality, value-based materials testing services to a thriving construction industry which (at the time) had limited and underperforming CoMET provider options.  I subsequently merged Terra Firma Testing with Northern Geotechnical Engineering in 2004 to form NGE-TFT. In my free time, I enjoy landscaping, home remodeling, live auctions, spending quality time with Charlie and Jezzy (my two prized Shelties), and of course, following my life-long hockey team the Montreal Canadiens (Go Habs!).</p>'
            },
            {   
                urlPath : '/people/bio/nbMartin',
                main: false,
                'pageTitle': 'Norman Brent Martin',
                'bioImage': 'bio_img_nbmartin.jpg',
                'backLink': '/people',
                'content': '<p>Howdy, Brent Martin here.  I moved to Alaska in 1990 as a welder for Asay Trucking to design and build steerable dollies to haul 190,000 lb. bridge girders.  I started my inspection career in 1998 with PND Engineering doing inspection on the four melt water bridges on the Arctic North Slope west of the Kuparuk operating field.  From 2000 to 2005 I worked with DOWL, LLC as a materials testing technician and special inspector.  In 2005 I started with NGE-TFT, bringing with me the start of special inspections for our company. </p>'
            },
            {
                urlPath : '/people/bio/mDean',
                main: false,
                'pageTitle': 'Mark Dean',
                'bioImage': 'bio_img_mdean.jpg',
                'backLink': '/people',
                'content': '<p>Hello, my name is Mark Dean.  I serve many roles at NGE-TFT, but my most demanding position is as acting Field Manager for our CoMET technicians and inspectors. I also am one of our Senior Special Inspectors and Materials Testing Technicians.  I have been with NGE-TFT since 2004, just as NGE and TFT were merging forces, and I have played a pivotal role in the growth of the company.  My primary focus is towards client service and satisfaction.</p><p>I have applied my trade all over the state of Alaska, from remote projects only accessible by boat or plane, to communities all along the Alaska road system.  My varied experiences have provided me with a unique understanding of the logistics associated with operating in this great state.   I have always been able to deal with unique situations and make quick, competent decisions to resolve problems that arise during the construction process.</p><p>I was born in Chicopee, Massachusetts, and relocated to Alaska in the winter on 1968 when my father was re-stationed by the Air Force.  Being raised in Alaska most of my life, I have had the privilege of watching this state “grow up”, and I am proud to call myself an Alaskan and I enjoy this great state that I call home.  I am a big outdoor enthusiast and love to hike, bike, ski, snowmachine, camp, and ride ATVs.  My other passions include tinkering with my 1978 Trans Am, traveling, music, movies, cooking, and of course Chicago Bears football – I never miss a game!</p>'
            },
            {
                urlPath : '/people/bio/aFortt',
                main: false,
                'pageTitle': 'Andrew F. Fortt, PhD',
                'bioImage': 'bio_img_afortt.jpg',
                'backLink': '/people',
                'content': '<p>My name is Andrew Fortt and I’ve had the fortune to work at NGE-TFT since 2012. At that time, I managed to convince my wife and one-year old daughter that it was in our best interests to pack up our belongings into the back of a truck and drive across the country from Vermont, to fill a job posting for a geotechnical engineering position that I had found advertised on Craigslist.</p><p>It turned out to be a great decision, and I have found my position at NGE-TFT to be enjoyable and rewarding. I serve as a project engineer at NGE-TFT and I am the manager of our materials testing laboratory. I’ve worked on a wide variety of geotechnical engineering projects including shallow and deep foundations, foundations on permafrost, liquefaction analyses, concrete mix designs, and passive refrigeration systems. I’ve conducted geotechnical field explorations for a variety of projects ranging from single family homes to gravel borrow mines, and I have travelled across Alaska to locations ranging from Point Hope to Unalaska. As laboratory manager, I am also responsible for the day-to-day operations and quality control of the laboratory. I also established our advanced testing laboratory which has enabled us to provide high-level material strength testing services.</p><p>Prior to working at NGE-TFT, I spent several years at Dartmouth College conducting research and advanced materials testing, specifically pertaining to the low temperature properties of materials. My experience included designing and fabricating laboratory equipment to measure the strength, friction, and thermal properties of materials. I served as a consultant for NASA’s return to flight program from 2004 to 2005. I am also the primary author of several peer reviewed papers on the mechanical behavior of ice.</p><p>I hold a Masters Degree in Mechanical Engineering Design from the University of Glasgow which I obtained in 2001. I subsequently moved to the United States to pursue a Doctoral Degree in Materials Science at Dartmouth College, which I received in 2006.</p><p>Outside of work I enjoy spending time with my wife and two young daughters, and we try to take advantage of all the great outdoor experiences that Alaska has to offer. I enjoy hunting and fishing and I am currently working on my recreational pilot’s license.</p>'
            },
            {
                urlPath : '/people/bio/aSmith',
                main: false,
                'pageTitle': 'Andrew C. Smith, CPG (AK #663)',
                'bioImage': 'bio_img_asmith.jpg',
                'backLink': '/people',
                'content': '<p>Hello, my name is Andrew Smith, and I am the Senior Geologist at NGE-TFT. I am a Certified Professional Geologist, licensed by the State of Alaska, and I have built my entire professional career in Alaska.  I received a B.S. in Geology from Stephen F. Austin State University in 1999 and a M.S. in Geology from Idaho State University in 2002.  I joined NGE-TFT in 2005, shortly after the company’s merger, and I feel that I have played a key role in the development of our company and the professional service that we provide.  I have filled a number of roles during my tenure with NGE-TFT, but my primary responsibilities currently center on project management and the development, coordination, and execution of our varied subsurface exploration and field testing programs.</p><p>I have served on remote projects all across Alaska, and I have developed a widespread understanding of the unique demands and limitations of conducting exploration and construction projects in rural Alaska; as well as in areas with harsh climates, difficult terrain, and limited access.  My specific project experiences span a range of geotechnical, geological, and environmental disciplines including the support of civil infrastructure projects, commercial/residential construction, natural resource development, oil/gas production, energy generation and distribution, and commercial fishing/processing.  I also have a strong technical writing background and I have extensive experience compiling technical documents and providing peer review.  I am an active member of the Geoprofessional Business Association (GBA) and am a graduate of GBA’s Fundamentals of Professional Practice program.  I am also a member of the Board of Directors for NGE-TFT.</p><p>I was raised in the Hill Country around Austin, Texas; and despite all of the good things that the Great State of Texas has to offer, I have always been attracted to more mountainous terrain and cooler climes.  This attraction was fostered during my youth through my involvement with the Boy Scouts, and during my early collegiate years through my participation in various geologic field studies across the southwestern US. After undergraduate school, I explored the Rockies and Intermountain West while living in Idaho and working on my graduate degree.  My curiosities eventually brought me to Alaska, where I now live with my lovely lady and our black lab in the Chugach Mountains along the Turnagain Arm of Cook Inlet.  While not at work, I try to spend as much time as possible in pursuit of my recreational passions which hinge around fishing, hunting, snow sports, and various other outdoor activities.  I am an avid sport and music fan, and I often travel in pursuit of both.  I try to complement my recreational and enthusiast pursuits with life’s more simple pleasures like good food and good drink, and I enjoy sharing them all with my friends and family who are scattered all across the country.</p>',
            },
            { 
                urlPath :'/people/bio/cKreitel',
                main: false,
                'pageTitle': 'Cody J. Kreitel, P.E.',
                'bioImage': 'bio_img_ckreitel.jpg',
                'backLink': '/people',
                'content': '<p>Hello, my name is Cody Kreitel. I am a senior project engineer at NGE-TFT and am licensed as a Professional Civil Engineer in the State of Alaska. I have worked in the civil engineering industry in Alaska since the summer of 2007, when I interned at DOWL Engineers, LLC in Anchorage, Alaska. During the summer of 2008, I interned at NGE-TFT and realized immediately that I preferred the small company environment and I felt at home here. After graduating from Brigham Young University in 2009 with a bachelor’s degree in Civil Engineering, I came to work at NGE-TFT full-time. While working full-time, I pursued a master’s degree in Arctic Engineering at The University of Alaska – Anchorage, which I completed in 2013. My undergraduate work focused on water resources and geotechnical engineering. My arctic engineering studies allowed me to apply these two skill sets to the unique challenges of arctic and sub-arctic environments.</p><p>I was privileged very early in my career to be tasked with the feasibility study and design of a reinforced concrete dam at a salmon hatchery on a remote island in Prince William Sound. With oversight from a professional engineer, I successfully designed and oversaw the construction of the dam. My design work included all of the geotechnical, hydrological, hydraulic, and structural components. I have also been privileged to work on a wide variety of other projects including but not limited to the design of deep foundations, retaining walls, drainage systems, permafrost foundations, foundation refrigeration systems, reinforced concrete dams, earthen dam spillways, marine sheet-pile bulkheads, off-shore oil and gas platforms, mining infrastructure, multi-story buildings, roads, bridges, and residential developments.</p><p>Over the years I have developed a strong ability to develop advanced, user friendly Excel spreadsheets using a combination of VBA programming and cell formulas/formatting. I have developed spreadsheets for design problems, administrative tasks, and laboratory testing which are used extensively by the entire company. The laboratory spreadsheets I have developed are more user-friendly and provide more functionality then most commercially available software.</p><p>I was born in Anchorage but I spent my high school years in northern Chihuahua, Mexico. After high school, I took two years off from college to serve as a volunteer, full-time missionary in the Czech Republic for the Church of Jesus Christ of Latter-day Saints. After completion of my mission, I realized Alaska was still in my blood and I couldn’t stay away.</p><p>Outside of work I am a devoted husband and father to my beautiful wife and two young children. I serve actively in my church and in the Boy Scouts of America. I enjoy as much of the Alaskan outdoors as possible through hunting, fishing, winter sports, camping, hiking, biking, rafting, boating, and any activity that gets me into the wilderness.</p>'
            },
            {
                urlPath : '/people/bio/cBanzhaf',
                main: false,
                'pageTitle': 'Clinton J. Banzhaf, P.E.',
                'bioImage': 'bio_img_cbanzhaf.jpg',
                'backLink': '/people',
                'content': '<p>Hello everyone, my name is Clinton J. Banzhaf. I am licensed as a Professional Civil Engineer in the State of Alaska and have been working with NGE-TFT since 2011, but let me start from the beginning of my life in Alaska. In 2000, my parents and I moved to this amazing state from Wyoming. Upon graduating from high school, I started on a path to become a mechanical engineer at the Colorado School of Mines (CSM), but I always had an interest in civil engineering. After my first year of school at CSM (and after receiving motivation and guidance from my college advisors) I decided to embark on the strenuous task of becoming both a mechanical and civil engineer. I graduated from CSM in December 2010 with Bachelor of Science degrees in both mechanical and civil engineering. In addition to my current duties at NGE-TFT, I am also currently pursuing a Master of Science degree in Arctic Engineering at The University of Alaska – Anchorage, and I am on track to graduate in 2016. The Arctic Engineering program, along with my varied experiences at NGE-TFT, has armed me with many unique arctic engineering skills.</p><p>Over the course of my life I have worked in almost every facet of construction there is, including: operating heavy equipment for various earthworks projects, pouring concrete, and wood/steel framing and roofing. During my college years, I returned to Alaska every summer to work for a geotechnical and environmental drilling company located in Anchorage. The drilling company that I worked for supports NGE-TFT on the majority of their geotechnical exploration projects, and one of these projects led to my eventual employment at NGE-TFT.  I have felt at home ever since. Since beginning my tenure with NGE-TFT, I have had the opportunity to hone my engineering skills with the construction skills that I developed in my younger years. I have had the privilege to work on and manage several challenging projects including pavement/road design; residential and commercial building foundations; tower foundations; permafrost foundations (North Slope); shallow and deep foundations; foundation repairs; pile design (pipe, pin, helical, freeze-back); offshore platform foundation; weigh stations; dam safety inspections; dam repair and enlargement; along with drilling for geotechnical explorations and environmental testing, just to name a few.</p><p>My wife, our three dogs, and I thrive on outdoor actives including: hiking, hunting, camping, fishing, kayaking, and off-roading.</p>'
            },
            {
                urlPath : '/people/bio/xGe',
                main: false,
                'pageTitle': 'Xiaoxuan Eva Ge, E.I.T.',
                'bioImage': 'bio_img_xge.jpg',
                'backLink': '/people',
                'content': '<p>Hello, my name is Xiaoxuan Ge, but you can call me Eva if your tongue is trembling to pronounce my Chinese name correctly (my coworkers can share endless experiences with you about that). I joined NGE-TFT in 2013, right after my graduation from the University of Alaska in Anchorage (UAA). I have served as a project engineer in our company for three years, and I hopefully will obtain the title of Senior Project Engineer after I attain my Professional Engineering license in the coming years.</p><p>My responsibilities in our company focus on a broad variety of task-specific geotechnical engineering projects, which include shallow and deep foundation design, retaining wall design, slope stability analysis, thermal analysis, pavement design, septic system design, pile p-y curve analyses, etc. I have also developed extensive experience providing quality control inspections for multiple projects located in remote parts of Alaska.</p><p>Prior to my tenure at NGE-TFT, I spent two years at UAA conducting research into deep foundation testing and the mechanical properties of frozen soils. I hold a Bachelor of Science Degree in Geological Engineering from the University of Jilin, in Jilin, China, which I received in 2011. At that time, I managed to convince my parents that their 22-year-old daughter, just out of college, and who had never traveled abroad before, would have no problem traveling alone to the United States to pursue a graduate education in civil engineering. Nevertheless, I successfully graduated from UAA in 2013 with a M.S. degree in Civil Engineering.  I now have my parents staying with me in Anchorage and they are joyful about their trip to America, which makes me feel that I made the right decision five years ago, and I am grateful about the life I have, and what I went through to get where I am.</p>'
            },
            { 
                urlPath : '/people/bio/sMcCoy',
                main: false,
                'pageTitle': 'Shelley A. McCoy, E.I.T.',
                'bioImage': 'bio_img_smccoy.jpg',
                'backLink': '/people',
                'content': '<p>My name is Shelley McCoy, and I have been working as a project engineer for NGE-TFT since 2012. My family moved to Alaska in 2001, and I attended high school in Glennallen, AK. From there, I began my college career at the University of Hawaii at Manoa, before transferring to the University of Alaska in Anchorage to complete my Bachelor of Science degree in Civil Engineering (2010). During, and immediately after college, I enjoyed several internships with different construction management companies in the Anchorage area; gaining experience in various disciplines such as construction as-built documentation, material procurement, and project budgeting.  I began my tenure with NGE-TFT after being referred to them by a close acquaintance who has worked with NGE-TFT in the past and who spoke highly of the company’s culture and work environment.</p><p>While working for NGE-TFT, the majority of my professional experience has come from directing and performing subsurface exploration programs and from designing pavement sections and shallow and deep foundations for projects throughout Alaska. These projects have taken me from Cordova, to Kodiak, to the North Slope oil fields; where I performed quality control inspections for earthworks and pile placement. I have also gained proficiency in various software applications such as AutoCAD (creating drawing sets for several projects), AllPile (computing lateral pile capacities), and GRLWEAP (performing wave equation analyses for pile driving).</p><p>When not working, I enjoy outdoor recreational activities with friends, such as rafting, hiking, and snowboarding. One of my passions is travel, and I have enjoyed extended trips to Australia, China, and Ireland, and I hope to continue to add to my list of destinations in the years to come.</p>'
            },
            {
                urlPath : '/people/bio/sTotzke',
                main: false,
                'pageTitle': 'Sean C. Totzke',
                'bioImage': 'bio_img_stotzke.jpg',
                'backLink': '/people',
                'content': '<p>Greetings, I am Sean Totzke, “The” Senior Inspector and Laboratory Technician with NGE-TFT. After 21 plus years of service with the United States Air Force I’ve turned my, knowledge, honesty, integrity, and commitment of excellence towards civil inspection and materials testing all over this great State of Alaska. I’m certified through; International Code Council (ICC), American Concrete Institute (ACI), the Municipality of Anchorage (MOA inspector #076), Western Alliance for Quality Transportation (WAQTC), National Institute for Certification in Engineering Technologies (NICET), and Troxler Nuclear Gauge Company. Over the past years I have worked on a wide variety of projects ranging from airports, hospitals, military structures, refineries, schools, housing, roads, single/multi-level private and public facilities in the field/laboratory environment and am now using my 15 years of civil Inspection and materials testing experience to assist our clients with known/unknown construction issues directly contributing to both their and our successes.</p><p>I grew up in a small town in southern Minnesota and in an environment that bred an avid passion for sports (Football, Darts, Hockey and Golf), music (Classic Rock, Classical, with a little Country), and travel (with my Mom being from Leeds, England). Heeding the call for more than small town life, I entered the service, graduating from USAF Basic Training in March of 1982. Since then I have traveled to 40 plus states and 20 plus countries, with Elmendorf AFB in Anchorage, Alaska being my second station in 1986. Through my travels no place has matched the fishing, skiing, scenery, or the great mix of people that I have found while living in “The Last Frontier”, so I decided to call it home. In my free time I still pursue my passions; watching the Vikings live, (Skol Vikings!) seeing concerts, (Roger Waters “The Wall” in New Zealand, Meet and greet with Alice Cooper in New York) and golfing as many “Pro” courses as possible (TPC Scottsdale, Disney Palm, Wolf Creek, The Glades) all while visiting family and friends all over our country and the world.</p>'
            },
            {
                urlPath : '/people/bio/jGray',
                main: false,
                'pageTitle': 'Joy Gray',
                'bioImage': 'bio_img_jgray.jpg',
                'backLink': '/people',
                'content': "<p>If it doesn't require an engineer to design it, a special inspector to sign off on it, or a technician to test it, I am the woman behind the scenes making sure NGE-TFT runs like a well-oiled machine. I am Joy Gray, Office Administrator and Human Resources Coordinator.  I came on board in 2010 after a short detour to the Pacific Northwest for three years.</p> <p>Currently, I serve on the board of directors for NGE-TFT, and I am corporate controller and oversee administration and accounting. In 2012, NGE-TFT became a partial employee-owned company, setting up an ESOP, where I played a crucial role in the implementation phase as well as ongoing oversight.</p><p>Although not a lifetime Alaskan, I consider Alaska my home and have been fortunate enough to spend half my life here. Raised in rural Kansas, when I wasn't working on my father's and grandfather's construction sites, I was exploring the hay and corn fields, climbing trees...  Later construction work took us to Alaska for a short period then California where we adapted to city life. I never forgot those short years in Alaska and returned after graduation. Chugiak has been my home since my return to Alaska.  I love the smaller town feel and enjoy being so close to our majestic mountains and lakes. When not at work, you can find me with my two children, our two Scottish terriers, and extended family, hiking in the mountains, paddle boarding on a nearby lake, indoor rock climbing, fishing, or a variety of other outdoor activities. I'm actively involved in religious volunteer work and with my local congregation.  Also, I am looking forward to my first hunt later this year!</p><p>I enjoy my work and look forward to seeing our company continue to grow. My philosophy at work:  efficiency and organization, yet have some fun.  In life, be positive, find some joy each day, continue to learn and challenge yourself and others, and enjoy art, music, and a great glass of wine on occasion.</p>"
            },
            {
                urlPath : '/people/bio/aGoodwin',
                main: false,
                'pageTitle': 'Aimee Goodwin',
                'bioImage': 'bio_img_agoodwin.jpg',
                'backLink': '/people',
                'content': '<p>Hello, my name is Aimee, and I am a geologist intern at NGE-TFT. I began my internship with NGE-TFT in 2014, and I intern at NGE-TFT on a part-time basis while I complete my undergraduate studies in Geology and Anthropology at the University of Alaska - Anchorage.  I previously earned a Bachelor of Science degree in Natural Sciences from the University of Alaska - Anchorage in 2014, and I have plans to pursue a master’s degree in geology once I complete my undergraduate studies.  My duties at NGE-TFT primarily center on computer drafting and field exploration activities.  I am the administrator for our exploration logging software (gINT) and have extensive experience with gINT and other exploration logging software programs. </p><p>I was born and raised in Anchorage, Alaska. I love it here and don’t plan on ever leaving. When I am not in class or at work I am keeping busy with my husband, our sons, our dogs, and in the community. I am a representative for the UAA Geology Club and I run three youth bowling leagues with the help of my husband. I enjoy bowling in the winter and softball in the summer. </p>'

            }
        ]
    }],
  
    'contact' : [{
        _layout : "contactLayout",
        urlPath:'/contact',
         header: 'contactHeader',
         bodyBg: '/img/bg_pattern_1_home.svg',
         showcaseBg : false
    }
    ],
    'projects' :[{
        _layout : 'projectsLayout',
        urlPath :'/projects',
        header:'projectsHeader',
        bodyBg: '/img/bg_pattern_1_projects.svg',
        showcaseBg: false,
         subLinks : [{
                        title: "Dams",
                        link: '/projects/geotechnical/dam'
                    }, {
                        title: "Dept. of Transportation",
                        link: '/projects/geotechnical/dpt'
                    }, {
                        title: "Docks",
                        link: '/projects/geotechnical/docks'
                    }, {
                        title: "Foundations",
                        link: '/projects/geotechnical/foundations'
                    }, {
                        title: "Geotech Investigations",
                        link: '/projects/geotechnical/geotech'
                    }, {
                        title: "Mines",
                        link: '/projects/geotechnical/mines'
                    }, {
                        title: "Offshore",
                        link: '/projects/geotechnical/offshore'
                    }, {
                        title: "Pipelines",
                        link: '/projects/geotechnical/pipelines'
                    }],
        subPages : [
            {
                urlPath : '/projects/geotechnical/dam',
                content : [{
                            title: 'Armin F. Koernig Hatchery Dam',
                            blurb: 'The Armin F. Koernig (AFK) Hatchery Dam provides all of the water supply to the AFK Hatchery. In 2008, NGE performed a periodic safety inspection of the original dam which was constructed during the 1930s and expanded in the 1970s. During the inspection it was determined that the dam had deteriorated significantly and was in need of repair or replacement. In 2009 NGE-TFT was contracted to perform a feasibility study for the repair or replacement of the dam. As a result of that study, the decision was made to replace the dam and ½-mile long above ground pipeline that provides the hatchery water supply. It was also decided to raise the normal reservoir level by approximately 5 feet. Design of the new dam began in 2010 and construction was completed in 2012. The new dam consists of a 23 ft tall post-tensioned concrete dam in the main stream channel with two sheetpile core earthen berms. The new dam had to be constructed without disrupting the water supply to the hatchery. Other project constraints include the remote location in Prince William Sound (air and sea access only) and frequent severe weather. NGE-TFT provided all of the design services including geotechnical, hydrologic/hydraulic, and structural as well as construction monitoring and materials testing.',
                            img: 'geo/afkdam.jpg'
                        }, {
                            title: 'Lake of the Hills Dam',
                            blurb: 'The Lake O’ the Hills dam is a privately owned dam that impounds a recreational lake. When the State of Alaska Dam<br/>Safety Program determined the existing corrugated metal pipe spillway was no longer fit for service due to insufficient capacity and pipe corrosion, NGE-TFT was contracted to design and construct a new spillway. Because the spillway was the only outlet for the reservoir, water levels were maintained during construction using a 6-in diameter siphon. A sheet pile weir and rip-rap protected spillway was designed to pass a flood flow of approximately 250 cfs.',
                            img: 'geo/lakehillsdam.jpg'
                        }]
               
            },
            {
                urlPath: '/projects/geotechnical/dpt',
                content: [{
                            title: 'Akutan Harbor Rd.',
                            blurb: 'Conducted a comprehensive geotechnical evaluation of the proposed road alignment to connect the Native Village of Akutan with the new small boat harbor (currently under construction at the head of Akutan Bay).  The remoteness and steep topography of Akutan Island required NGE-TFT to employ less traditional exploration methods including ground-penetrating radar and portable hand augering to evaluate bedrock depths and overburden thicknesses.  Subsurface and material information obtained by NGE-TFT, as well as past experiences with construction practices on the island, helped to guide road design and result in the overall shortening of the road alignment, saving the project considerable construction cost.',
                            img: 'geo/akutanharborrd.jpg'
                        }, {
                            title: 'AK DOT Jim River Maintenance Facility',
                            blurb: 'NGE-TFT conducted a geotechnical evaluation of the site to characterize subsurface conditions (which included ice-rich permafrost) and develop foundation recommendations for the proposed structure.  As a part of the foundation design effort, NGE-TFT conducted a transient thermal analysis to model the effect that the proposed building could have on the existing permafrost and to evaluate the potential effectiveness of proposed foundation refrigeration systems and insulation in preserving the existing permafrost beneath and adjacent to the proposed structure.',
                            img: 'geo/jimriver.jpg'
                        }, {
                            title: 'Bethel Readiness Center',
                            blurb: "NGE-TFT conducted a geotechnical evaluation of two separate sites to characterize subsurface conditions (which included marginal/warm permafrost) and develop foundation recommendations for the proposed structure.  As a part of the foundation design effort, NGE-TFT conducted thermal monitoring at the selected site to evaluate the condition of the permafrost.",
                            img: 'geo/bethelrc.jpg'
                        }]
            },
            {
                urlPath : '/projects/geotechnical/docks',
                content : [{
                            title: 'Akutan East Dock Expansion',
                            blurb: "NGE-TFT provided design services, and on-site engineering support and construction management to the construction of a 1,000-ft long sheet pile dock wall at Trident Seafood’s processing facility located in Akutan, Alaska.  Services provided included a geotechnical evaluation of the dock wall alignment, sheet pile wall and tieback anchor design, and backfill recommendations.  A borrow source evaluation and volume determination was also conducted in association with the dock design which evaluated the suitability of a nearby quarry to produce backfill material for the proposed dock expansion.",
                            img: 'geo/akutaneastdock.jpg'
                        }, {
                            title: 'Akutan West Dock Repair',
                            blurb: "NGE-TFT provided design services and on-site engineering support to repair a failed sheet pile bulkhead wall at Trident Seafood’s processing facility located in Akutan, Alaska.  Services provided included sheet pile wall design, including drilled and grouted tieback anchors and crane and bollard foundations",
                            img: 'geo/akutanwestdock.jpg'
                        }, {
                            title: 'Port Mackenzie Barge Dock Expansion',
                            blurb: 'Geotechnical evaluation performed to monitor and assess a vibro-compaction effort for densification of earthen fill dike within the guidelines set by the Project Engineer.  Work included SPT sampling before and after compaction effort and SPT hammer efficiency determination utilizing SPT analyzer equipment.',
                            img: 'geo/portmacdock.jpg'
                        }]
            },{
                urlPath : '/projects/geotechnical/foundations',
                content: [{
                            title: 'Eklutna Power Generations Station',
                            blurb: 'NGE-TFT conducted a two-phase geotechnical exploration program at the site of a proposed electrical power generation station located in Eklutna, Alaska. The evaluation consisted of a series of subsurface borings aimed at identifying the conditions across the site, including quantifying if any areas of residual permafrost existed across the site. Temperature acquisition cables (a.k.a. thermistor strings) were installed at select boring locations across the site in order to monitor ground temperatures and help confirm that no isolated areas of permafrost exist within the footprint of the proposed facilities. Foundation recommendations were developed for each of the proposed facilities in close cooperation with the structural design team. NGE-TFT was also contracted to perform extensive infiltration testing and groundwater level monitoring across the site in support of the surface drainage design for the site.',
                            img: 'geo/eklutnagenstn.jpg'
                        }, {
                            title: 'Verizon Wireless Alaska Cellular Network',
                            smallerTitle : true,
                            blurb: 'NGE-TFT conducted a series of geotechnical explorations at the sites of Verizon’s proposed cellular network towers in areas surrounding Fairbanks, Anchorage, and Juneau, Alaska.  The explorations consisted of both geotechnical borings and electrical resistivity measurements.  NGE-TFT developed foundation recommendations for both tower and support communication modules.',
                            img: 'geo/verizon.jpg'
                        }]
            },{
                urlPath : '/projects/geotechnical/geotech',
                content: [{
                            title: 'Halliburton Prudhoe Bay Facility Improvements',
                            smallerTitle : true,
                            blurb: 'NGE-TFT provided engineering design and construction inspection to two separate phases of construction at Halliburton’s Prudhoe Bay facility in Deadhorse, Alaska.  NGE-TFT provided recommendations for freeze-back pile foundations for the proposed Nitrogen Plant, Sperry Shop, and Truck Maintenance facilities constructed on-site.  NGE-TFT also provided on-site inspection of the pile installation activities, including logging of excess ice in each pile pilot hole, observation of slurry activities, and thermal monitoring of slurry freeze-back.  The on-site inspection was critical in providing real-time engineering design modifications based on the amount of ice encountered at each pile site.  Thermal monitoring of the slurried piles also allowed for more accurate determinations of pile loading.',
                            img: 'geo/halliburtonprudhoe.jpg'
                        }, {
                            title: 'McGrath Regional Health Clinic',
                            blurb: 'NGE-TFT was contracted to provide a geotechnical evaluation for the design of the proposed health center and associated parking area. Final geotechnical engineering recommendations for the site were developed based on preliminary subsurface explorations conducted at the site, which identified the presence of permafrost within the proposed building footprint and resulted in a re-configuration of the site design. NGE-TFT also provided construction oversight during the foundation construction and cut/fill activities at the site.',
                            img: 'geo/mcgrathhealth.jpg'
                        }, {
                            title: 'Terraces Subdivision',
                            blurb: 'Provided preliminary geotechnical analysis and recommendations on Slope Stability for 500 home development.',
                            img: 'geo/terraces.jpg'
                }]
            },{
                urlPath : '/projects/geotechnical/mines',
                content : [{
                            title: 'Livengood Gold Mine',
                            blurb: 'NGE-TFT provided field, laboratory, and engineering support to a feasibility-level geotechnical exploration effort for a proposed open-pit gold mine in Livengood, Alaska. Of particular importance to the design evaluation was the need to obtain representative samples of undisturbed permafrost in order to help quantify the nature of the permafrost within the proposed facilities. This was accomplished through the use of refrigerated brine drilling technology, which employed a brine drill mud mixture which was refrigerated onsite to approximately 10°F and then circulated down each exploration core hole to help preserve the permafrost while providing adequate lubrication and cuttings removal. The samples were preserved in a frozen state and delivered to our Anchorage laboratory where extensive thaw strength testing was conducted on the frozen samples.',
                            img: 'geo/livengood.jpg'
                        }, {
                            title: 'Rock Creek Gold Mine',
                            blurb: 'NGE-TFT conducted a series of coupled thermal/hydraulic transient analyses to model the impact of proposed and existing mine operations on the existing soil and hydraulic conditions.  Modeling was conducted using a numerical finite-element analysis software suite input with a database consisting of both measured and theoretical material properties.',
                            img: 'geo/rockcreek.jpg'
                        }]

            },{
                urlPath : '/projects/geotechnical/offshore',
                content : [{
                            title: 'Furie Platforms',
                            blurb: 'In Cook Inlet, jack-up platforms are being used to drill exploratory wells in search of oil and gas resources. NGE-TFT worked closely with Tester Drilling to modify an existing drill rig to be placed on the jack-up platform’s drilling deck for the geotechnical exploration. A conductor pipe was set just below mud line to protect the drilling rods from the swift tides of Cook Inlet. The deepest coring was 292 feet below mud line as limited by permitting. The cores were returned to NGE-TFT for testing to evaluate important soil parameters and obtain an appropriate strength of the material. Several triaxial strength tests were performed on intact samples and remolded samples that were deformed from transportation. NGE-TFT provided the geotechnical recommendations for the each project site to optimize the foundation design. The latest design consisted of 8 pipe piles, 42 inches in diameter, placed on a 45 foot diameter circle around a 24 foot diameter pile that extends from just below mud line out of the ocean to support the production platform.',
                            img: 'geo/livengood.jpg',
                            hideSlideControls : true
                        }]
            },
            {
                urlPath : '/projects/geotechnical/pipelines',
                content : [{
                            title: 'Alyeska Pipeline Strategic Reconfiguration',
                            smallerTitle : true,
                            blurb: 'NGE-TFT performed preliminary geotechnical evaluations and developed foundation recommendations for pile design efforts in association with the Strategic Reconfiguration efforts at five of 12 pump stations along the TransAlaska oil pipeline.  NGE-TFT also provided engineering oversight during pile installation at each of the five pump stations.  NGE-TFT also designed a septic effluent outfall system for Pump Station #4 which included a thermal analysis and monitoring of the proposed outfall system as well as construction oversight for outfall system.',
                            img: 'geo/alyeskapipeline.jpg'
                        }, {
                            title: 'SEIC Phase II – Onshore Pipeline System, Sakhalin, Russia',
                            blurb: 'NGE-TFT provided on-site geotechnical engineering expertise to 17 separate fault crossings along the 785 km oil and gas pipeline construction effort on Sakhalin Island, Russia.  Duties consisted of slope stability evaluations, drainage adequacy determinations, and right-of-way re-instatement and special trench construction monitoring and inspection.',
                            img: 'geo/seicphase2.jpg'
                        }]
            },
            {
                _layout : 'projectsLayout',
                'urlPath' : '/projects/special_inspection',
                header : "projectsHeader",
                bodyBg: '/img/bg_pattern_1_projects.svg',
                showcaseBg: false,
                subLinks :[{
                        title: "Education Facilities",
                        link: '/projects/special_inspection/edu'
                    }, {
                        title: "Government",
                        link: '/projects/special_inspection/gov'
                    }, {
                        title: "Medical Facilities",
                        link: '/projects/special_inspection/med'
                    }, {
                        title: "Natural Resources",
                        link: '/projects/special_inspection/nat'
                    }, {
                        title: "Private Sector",
                        link: '/projects/special_inspection/private'
                    }, {
                        title: "Subdivisions",
                        link: '/projects/special_inspection/subd'
                    }, {
                        title: "Transportation",
                        link: '/projects/special_inspection/trans'
                }],
                subPages : [
                    {
                        urlPath : '/projects/special_inspection/edu',
                        content : [{
                            title: 'UAA Science Facility',
                            blurb: '125,000 sf. 4-story concrete and steel structure.   Performed special inspection of all structural items. Also performed concrete, soils, and asphalt testing. Client: University of Alaska Anchorage',
                            client: 'University of Alaska Anchorage',
                            cost: '$75,000,000',
                            img: 'spl/uaascience.jpg'
                        }, {
                            title: 'UAA Sports Arena',
                            blurb: '196,000 sf multi-use facility that will house a 5,000 seat performance gymnasium for basketball & volleyball; a practice & performance gym for the gymnastics program; support space consisting of a fitness & training room, administration/coaching offices, laundry, A/V production, locker & team rooms for basketball, volleyball, gymnastics, skiing, track & cross country programs. Concrete and steel structure. NGE-TFT currently performing special inspection and materials testing. Client: University of Alaska Anchorage',
                            client: 'University of Alaska Anchorage',
                            cost: '$109,000,000',
                            img: 'spl/uaasports.jpg'
                        }]
                    },
                    {
                        urlPath : '/projects/special_inspection/gov',
                        content : [{
                            title: 'JBER TEMF– Unit Company',
                            blurb: 'This project required us to sample, store and test several hundred concrete beams and cylinders.',
                            img: 'spl/jber.jpg',
                            hideSlideControls : true
                        }]
                    },
                    {
                        urlPath : '/projects/special_inspection/med',
                        content : [{
                            title: 'Anchorage Native Primary Care Center',
                            blurb: '80,000 sf 3-story concrete and steel structure. NGE-TFT performed special inspections of all structural items. Also performed concrete, soils and asphalt testing. Client: South Central Foundation',
                            client: 'South Central Foundation',
                            cost: '$29,000,000',
                            img: 'spl/anchnativeprimary.jpg'
                        }, {
                            title: 'Providence Alaska Medical Center, Regional Administration Building',
                            smallerTitle : true,
                            blurb: '4-story structure with reinforced concrete inspection of footings, epoxy anchors, and fiber reinforced epoxy coated (frp) walls, and spray-applied fireproofing. Performed concrete, soils, asphalt testing and all special inspections. Client: Providence Alaska Medical Center',
                            client: 'Providence Alaska Medical Center',
                            cost: '$28,000,000',
                            img: 'spl/provadmin.jpg'
                        }, {
                            title: 'Providence Alaska Medical Center Generations',
                            smallerTitle : true,
                            blurb: 'Renovate 100,789 sq ft and construct 85,782 sq ft.  Affects NICU, Prenatal and Mother-Baby Units, surgery, pharmacy, sterile processing, and materials management.  (2011-2014) Performing special inspection and materials testing. Client: Providence Alaska Medical Center',
                            client: 'Providence Alaska Medical Center',
                            cost: '$150,300,000',
                            img: 'spl/provgen.jpg'
                        }, {
                            title: "VA Medical Center",
                            blurb: '162,000sf. 2-story concrete and steel structure.   Performed special inspection of all structural items including floor flatness testing. Also performed concrete, soils, and asphalt testing. Client: Department of Veteran’s Affairs',
                            client: "Department of Veteran's Affairs",
                            cost: ' $78,000,000',
                            img: 'vamedctr.jpg'
                        }]
                    },
                    {
                        urlPath : '/projects/special_inspection/nat',
                        content : [{
                            title: 'Colville Tank Farm – Prudhoe Bay',
                            blurb: 'Performed concrete, soils testing, special inspections on structure.',
                            img: 'spl/colvilletank.jpg'
                        }, {
                            title: 'Low Sulfur Diesel Refinery, Valdex',
                            img: 'spl/lowsulfurrefinery.jpg'
                        }, {
                            title: 'Southcentral Power Plant',
                            img: 'spl/southcentralpower.jpg'
                        }]
                    },
                    {
                        urlPath : '/projects/special_inspection/private',
                        content : [{
                                title: 'Aleut Pribilof Island Association Office',
                                blurb: '3-story braced frame steel structure with concrete footings and concrete slabs on metal deck and grade.  Performed Special Inspection of steel, welding, concrete, and spray applied fireproofing.',
                                client: "Venture Development",
                                cost: '$12,000,000',
                                img: 'spl/aleutprib.jpg'
                            }, {
                                title: 'Alutiiq Center',
                                blurb: '5-story structure with pre-stressed reinforced concrete and separate 2-story post-tensioned parking garage. Performed special inspection of all structural items. Also performed concrete, soils, and asphalt testing.',
                                client: 'ASCG',
                                cost: '$10,000,000',
                                img: 'spl/alutiiq.jpg'
                            }, {
                                title: 'Anchorage Museum Expansion: 2007-2009',
                                blurb: '81,000 sf 4-story concrete and steel structure. NGE-TFT performed special inspections on all structural items.',
                                cost: "$58,000,000",
                                img: 'spl/anchmuseum.jpg'
                            }, {
                                title: 'Anchorage Native Primary Care Center Parking Garage',
                                blurb: '173,000 sf. 5-story post-tensioned concrete parking structure. Performed all special inspection of structural items. Also performed concrete, soils, and asphalt testing.',
                                client: 'South Central Foundation',
                                cost: "$12,000,000",
                                img: 'spl/anchnativegarage.jpg'
                            }, {
                                title: 'Anchorage Rental Car Center',
                                blurb: '4-story post-tensioned concrete parking structure with two stand alone steel structures, four exterior ramps, exterior fueling station, and three interior car washes.  Performed all Special Inspection of structural items.  Also performed concrete, soils, and asphalt testing.',
                                client: 'South Central Foundation',
                                cost: "$12,000,000",
                                img: 'spl/anchrentalcarctr.jpg'
                            }, {
                                title: 'Eklutna Estates',
                                blurb: '92,000 sq.ft. 4 story assisted living concrete, post tension and wood framed.',
                                img: 'spl/eklutnaestates.jpg'
                            }, {
                                title: 'Embassy Suites Hotel',
                                blurb: '400,000 sf. 4-story mixed use concrete, steel, and wood structure.  Structural steel beams and columns for first floor and wood framing for remaining top three floors.  Performed special inspection of all structural items.',
                                client: 'Stonebridge Companies',
                                cost: '$20,000,000',
                                img: 'spl/embassysuites.jpg'
                            }, {
                                title: 'Park Plaza II Luxury Apartments',
                                blurb: '6-story mixed use concrete, steel, and wood structure.  Concrete basement with structural steel beams and columns for first two floors and wood framing for remaining top four floors.  Performed special inspection of all structural items.  Also performed concrete, soils, and asphalt testing.',
                                client: 'Weidner Properties',
                                cost: '$25,000,000',
                                img: 'spl/parkplazaapts.jpg'
                            }, {
                                title: 'Regal 16 Theater – Tikahtnu',
                                blurb: '101,000sf. Multi-level concrete and steel structure. Performed all structural and civil inspections. Including materials testing for concrete, soils and asphalt.',
                                client: 'Browman Development',
                                cost: '$20,000,000',
                                img: 'spl/regal16.jpg'
                            },
                            {
                                title: 'Target Store',
                                blurb: '200,000sf big box stores. Performed special inspection of all structural items. Also performed concrete, soils, and asphalt testing.',
                                client: 'Target Corporation'
                            }
                        ]
                    },{
                        urlPath : '/projects/special_inspection/subd',
                        header : 'projectsHeader',
                        content : [{
                            title: 'Terraces Subdivision',
                            hideSlideControls : true,
                            blurb: 'Provided preliminary geotechnical analysis and recommendations on Slope Stability for 500 home development.',
                            img: 'spl/terraces.jpg'
                        }]
                    },{

                        urlPath : '/projects/special_inspection/trans',
                        header : 'projectsHeader',
                        content : [{
                            title: 'Anchorage International Airport Retrofit Phase I and II',
                            smallerTitle : true,
                            blurb: '3-story concrete and steel structure. Performed special inspection of all structural items. Also performed concrete, soils, and asphalt testing.',
                            client: 'PCL Construction',
                            cost: '$75,000,000',
                            img: 'spl/anchintlretrofit.jpg'
                        }, {
                            title: 'Anchorage International Airport Seismic Upgrades',
                            blurb: 'Seismic upgrades to the existing Anchorage International Airport Concourse A.  including the added steel brace frames and thickened concrete shear walls. Performed Special Inspection of all steel structural items.',
                            img: 'spl/anchintlseismic.jpg',
                            client: 'PCL Construction',
                            cost: '$50,000,000'
                        }, {
                            title: 'Kuparuk Runway',
                            blurb: 'Kuparuk, AK',
                            client: "ConocoPhillips",
                            img: 'spl/kuparukrunway.jpg'
                        }]
                    }
                ]

            }
        ]
    }],
    'faq':[{
        _layout : 'projectsLayout',
        urlPath : '/faq',
        header : 'peopleHeader',
        bodyBg: '/img/bg_pattern_1_home.svg',
        showcaseBg:false,
        subPages:[{
            pageTitle: 'Our Service',
            url: '/faq/resource'
        }]
    }],
    'upload':[{
        _layout : 'contactLayout',
        urlPath :'/upload',
        main : 'simpleUpload',
        bodyBg: '/img/bg_pattern_1_home.svg',
        showcaseBg:false
    }
    ],
    'users' : [{
        _layout : 'contactLayout',
        urlPath:'/users',
        bodyBg : '/img/bg_pattern_1_home.svg',
        showcaseBg : false
    }],
    'services': [
        {
            _layout : "aboutLayout",
            urlPath: '/services',
            header: 'servicesHeader',
            //bodyBg: 'bg_services_construction_site_materials_testing',
            pageTitle: 'Our Service',
            main:'services',
            indexBlurb : "We specialize in the testing of soils, rock, and engineered construction materials; including aggregate, concrete, and asphalt, for the geotechnical engineering and construction and mining industries. We pride ourselves upon our ability to provide accurate and timely results for our clients. With over 18 years in business, we have the experience that enables us to look at the bigger picture of your project, and help you determine the tests required to provide the most cost effective solution to your project’s testing needs",
            'subLinks': [{
                title: 'Construction Site Materials Testing and Observation',
                link: '/services/construction_site_materials_testing',
            }, {
                title: 'Geotechnical Engineering',
                link: '/services/geotechnical_engineering',
            }, {
                title: 'ICC Special Inspection',
                link: '/services/icc_special_inspection',
            },{
                title: 'Lab Materials Testing',
                link: '/services/lab_materials_testing'
            }]
        },
        {
        _layout : "aboutLayout",
        urlPath: '/services/construction_site_materials_testing',
        header: 'servicesHeader',
        bodyBg: 'bg_services_construction_site_materials_testing',
        pageTitle: 'Construction Site Materials Testing and Observation',
        keywords : [
            'Compaction',
            'Density',
            'Asphalt',
            'Concrete',
            'Grout',
            'ACI',
            'WAQTC',
            'Bottom-of-hole',
            'Soils'
        ]
    }, {
        _layout : 'aboutLayout',
        urlPath: '/services/geotechnical_engineering',
        keywords : [
            'Exploration',
            'Drilling',
            'Foundation',
            'Slope Stability',
            'Seismic',
            'Pavement',
            'Retaining wall',
            'Drainage',
            'Permafrost',
            'Thermal modeling',
            'Dam inspection',
            'Anchors',
            'Sheet pile',
            'Infiltration',
            'Percolation',
            'Slurry',
            'Ad-freeze'
        ],
        pageTitle: 'Geotechnical Engineering',
        header: 'servicesHeader',
        subPages: [{
                urlPath: '/services/geotechnical_engineering/subsurface_exploration',
                pageTitle: 'Subsurface Exploration, Sampling and Monitoring',
                header: 'servicesHeader',
                'subLinkIntro': '<h4>Click on the services below for detailed information about each item.</h4><br/>',
                'subLinks': [{
                    title: 'Soil and Bedrock Exploration Methods',
                    link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                }, {
                    title: 'Soil and Rock Sampling Methods',
                    link: '/services/geotechnical_engineering/soil_sampling_methods'
                }, {
                    title: 'Specialty Exploration Method Deployment',
                    link: '/services/geotechnical_engineering/specialty_exploration_method'
                }, {
                    title: 'Geophysical Subsurface Exploration and Testing',
                    link: '/services/geotechnical_engineering/geophysical_exploration'
                }, {
                    title: 'Instrumentation and Monitoring',
                    link: '/services/geotechnical_engineering/instrumentation'
                }],
                subPages: [{
                    urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock',
                    pageTitle: 'Soil and Bedrock Exploration Methods',
                    isSlideshow: true,
                    subPages: [{
                        urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow',
                        pageTitle: 'Hollow-Stem and Solid Flight Auger Drilling',
                         content: [{
                                        title: "Hollow-Stem Image 1",
                                        img: 'slides/hollow_stem_1.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Hollow-Stem Image 2',
                                        img: 'slides/hollow_stem_2.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Hollow-Stem Image 3',
                                        img: 'slides/hollow_stem_3.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Hollow-Stem Image 4',
                                        img: 'slides/hollow_stem_4.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Hollow-Stem Image 5',
                                        img: 'slides/hollow_stem_5.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Hollow-Stem Image 6',
                                        img: 'slides/hollow_stem_6.jpg',
                                        hideSlidesTitles : true
                                    }],
                        outerBlurb: "Hollow-stem auger drilling is a relatively fast, easy, and cost-effective geotechnical exploration technology which employs hollow drilling augers (with center drill rods) to advance a soil boring in relatively unconsolidated/non-lithified soil and/or decomposed rock down to depths of up to effective of approximately 150-300 feet (depending upon the subsurface conditions encountered).  The hollow augers support the borehole wall from collapse while serving as a conduit to allow for the advancement and retrieval of various geotechnical soil and rock sampling tooling and/or subsurface instrumentation.  Hollow-stem auger drilling is a proven and established geotechnical exploration method, and has proven to be the gold-standard for conventional geotechnical exploration programs.<br/><br/>Hollow-stem auger drilling does have limitations though, and proper considerations should be made prior to the commencement of a hollow-stem auger drilling program in order to evaluate its suitability, benefits, and risks for a given project.",
                         subLinks :[{
                                title: "Hollow-Stem and Solid Flight Auger Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                            }, {
                                title: "Air and Mud Rotary Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air'
                            },
                             {
                                 title: "Direct-push Technology",
                                 link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct'
                             },
                             {
                                title: "Sonic Drilling Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic'
                            }, {
                                title: "Diamond Rock Core Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond'
                            }, {
                                title: "Test Pit Excavation",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit'
                            }]
                             
                    }, {
                        urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air',
                        pageTitle: 'Air and Mud Rotary Drilling',
                        content: [{
                                        title: "Air Image 1",
                                        img: 'slides/air_mud_1.jpg',
                                        hideSlidesTitles : true

                                    }, {
                                        title: 'Air Image 2',
                                        img: 'slides/air_mud_2.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Air Image 3',
                                        img: 'slides/air_mud_3.jpg',
                                        hideSlidesTitles : true
                                    }],
                        outerBlurb: "Rotary drilling is a technique which employs a rotating drill bit that grinds/pulverizes the soil/rock as the drill bit advances and then uses a medium (such air compressed air, water, or drilling mud) to transport the drill cuttings up to the ground surface. Rotary drilling is most commonly associated with water and oil well drilling, but also plays an important role in geotechnical exploration.  Rotary drilling often incorporates the usage of driven steel casing to help reinforce the drilled borehole and prevent borehole collapse, while facilitating cuttings transport out of the borehole. Rotary drilling creates a borehole (either cased or open-hole) which serves as a conduit to allow for the advancement and retrieval of various geotechnical soil and rock sampling tooling and/or subsurface instrumentation.<br/><br/>Rotary drilling is especially useful for drilling in very coarse and/or dense soils, as well as bedrock, as it is relatively unaffected by the presence of hard materials (such as rock). Mud rotary drilling is also very useful for collecting undisturbed samples of sand soils which are located below the groundwater table, as the drilling mud serves to equalize the hydrostatic pressures between the open borehole and the surrounding formation, which reduces the potential for sand heave and increases the potential for the collection of an undisturbed sample (important for soil liquefaction analysis). However, rotary drilling requires an additional level of effort and support equipment and therefore can be relatively expensive to conduct and/or difficult to mobilize to remote areas",
                         subLinks :[{
                                title: "Hollow-Stem and Solid Flight Auger Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                            }, {
                                title: "Air and Mud Rotary Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air'
                            },
                             {
                                 title: "Direct-push Technology",
                                 link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct'
                             },
                             {
                                title: "Sonic Drilling Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic'
                            }, {
                                title: "Diamond Rock Core Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond'
                            }, {
                                title: "Test Pit Excavation",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit'
                            }]

                    },{
                        urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct',
                        pageTitle: 'Direct-push Technology',
                        content: [{
                            title: "Direct Image 1",
                            img: 'slides/direct_1.jpg',
                            hideSlidesTitles : true

                        }, {
                            title: 'Direct Image 2',
                            img: 'slides/direct_2.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'Direct Image 3',
                            img: 'slides/direct_3.jpg',
                            hideSlidesTitles : true
                        }],
                        outerBlurb: 'Direct-push exploration equipment "push" tooling (sampling equipment, etc.) and select geotechnical sensors into the subsurface soils without the use of rotational drilling. Direct push technology typically relies on a relatively small amount of static weight combined with percussive force as the energy for advancement of downhole tooling. Direct-push technology can provide continuous, relatively undisturbed soil samples with minimal ground disturbance on an equipment platform which is generally smaller and more portable than more conventional “rotational” drill rigs.  Their portability makes them excellent candidates for remote and/or air-supported operations off of established road systems.  Direct-push has limitations though, and is best suited for shallow exploration (<50 feet) in relatively fine-grained and thawed soils.  Furthermore, direct-push technology does not allow for the collection of several standard geotechnical sample tooling such as SPT, MPT, and/or Shelby tube.',
                        subLinks :[{
                            title: "Hollow-Stem and Solid Flight Auger Drilling",
                            link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                        }, {
                            title: "Air and Mud Rotary Drilling",
                            link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air'
                        },{
                            title: "Direct-push Technology",
                            link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct'
                        },
                            {
                            title: "Sonic Drilling Technology",
                            link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic'
                        }, {
                            title: "Diamond Rock Core Drilling",
                            link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond'
                        }, {
                            title: "Test Pit Excavation",
                            link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit'
                        }]

                    },
                        {
                        urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic',
                        pageTitle: 'Sonic Drilling Technology',
                        content: [{
                                        title: "Sonic Image 1",
                                        img: 'slides/sonic_1.jpg',
                                        hideSlidesTitles : true,
                                        hideSlideControls : true
                                    }
                        ],
                        outerBlurb : 'Sonic drilling is a subsurface exploration technique that strongly reduces friction on the drill string and drill bit due to liquefaction, inertial effects, and a temporary reduction of the porosity of the soil generated by a high-frequency vibration applied directly to the drill stem at the drill head. In addition to vibration, sonic drilling uses both the rotation and downforce of the drill rig and drill casing to advance the borehole. Sonic drill stems incorporate both an inner core barrel and an outer sonic drill casing to penetrate the substrate, stabilize the borehole, and collect continuous, relatively undisturbed soil samples. <br/><br/>Sonic drilling techniques can recover a fairly large sample specimen (depending upon the casing diameter employed), successfully sample large-diameter granular soils (gravel and cobbles), and are somewhat effective at recovering in-tact samples of frozen soils.  Sonic drilling is also fairly efficient, results in limited ground disturbance, and is devoid of drill cuttings.  Sonic drill rigs can also be equipped with standard geotechnical sampling equipment (SPT/MPT, Shelby tubes, etc.). However, sonic drilling is a fairly new, equipment-intensive technology, and therefore, is generally fairly cost-prohibitive for moderate to small-scale geotechnical exploration projects.',

                         subLinks :[{
                                title: "Hollow-Stem and Solid Flight Auger Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                            }, {
                                title: "Air and Mud Rotary Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air'
                            },
                             {
                                 title: "Direct-push Technology",
                                 link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct'
                             },
                             {
                                title: "Sonic Drilling Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic'
                            }, {
                                title: "Diamond Rock Core Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond'
                            }, {
                                title: "Test Pit Excavation",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit'
                            }]
                         
                    }, {
                        urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond',
                        pageTitle: 'Diamond Rock Core Drilling',
                        content: [{
                                        title: "Diamond Image 1",
                                        img: 'slides/diamond_core_1.jpg',
                                        hideSlidesTitles : true

                                    }, {
                                        title: 'Diamond Image 2',
                                        img: 'slides/diamond_core_2.jpg',
                                        hideSlidesTitles : true
                                    }, {
                                        title: 'Diamond Image 3',
                                        img: 'slides/diamond_core_3.jpg',
                                        hideSlidesTitles : true
                                    },{
                                        title: 'Diamond Image 4',
                                        img: 'slides/diamond_core_4.jpg',
                                        hideSlidesTitles : true
                                    },{
                                        title: 'Diamond Image 5',
                                        img: 'slides/diamond_core_5.jpg',
                                        hideSlidesTitles : true
                                    }],
                        outerBlurb : 'Diamond bedrock core drilling uses a diamond-impregnated coring bit to collect undisturbed rock core samples from bedrock.  The drill stem typically consists of an inner core barrel and outer drill steel which allows for the retrieval of individual rock core intervals without the removal of the entire drill stem from the corehole.  Drilling fluids (water, mud, etc.) are circulated down the inside of the drill steel and out of the end of the drill bit to help reduce friction at the drill bit and remove drill cuttings from the corehole. Rock core can be collected in a variety of diameters depending upon the goal(s) of the exploration program and the capabilities of the individual drill rigs.<br/><br/>Diamond rock core drilling is typically employed in geotechnical exploration applications when samples of the bedrock need to be evaluated for engineering properties.  Diamond rock core drilling can be very time consuming and expensive, and is usually reserved for projects which aim to utilize the bedrock as a source of construction materials (i.e. quarry, mine, etc.) or for projects which will bear directly on, or into, the bedrock body itself.',
                        subLinks :[{
                                title: "Hollow-Stem and Solid Flight Auger Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                            }, {
                                title: "Air and Mud Rotary Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air'
                            },
                            {
                                title: "Direct-push Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct'
                            },
                            {
                                title: "Sonic Drilling Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic'
                            }, {
                                title: "Diamond Rock Core Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond'
                            }, {
                                title: "Test Pit Excavation",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit'
                            }]
                     
                    }, {
                        urlPath: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit',
                        pageTitle: 'Test Pit Excavation',
                        main : 'geotech_soil_and_bedrock_test_pit',
                        content: [{
                            title: "Diamond Image 1",
                            titleOverride : '',
                            img: 'slides/test_pit_1.jpg',
                            hideSlidesTitles : true

                        }, {
                            title: 'Diamond Image 2',
                            img: 'slides/test_pit_2.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'Diamond Image 3',
                            img: 'slides/test_pit_3.jpg',
                            hideSlidesTitles : true
                        },{
                            title: 'Diamond Image 4',
                            img: 'slides/test_pit_4.jpg',
                            hideSlidesTitles : true
                        },{
                            title: 'Diamond Image 5',
                            img: 'slides/test_pit_5.jpg',
                            hideSlidesTitles : true
                        }],
                        subLinks :[{
                                title: "Hollow-Stem and Solid Flight Auger Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/hollow'
                            }, {
                                title: "Air and Mud Rotary Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/air'
                            },
                            {
                                title: "Direct-push Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/direct'
                            },
                            {
                                title: "Sonic Drilling Technology",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/sonic'
                            }, {
                                title: "Diamond Rock Core Drilling",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/diamond'
                            }, {
                                title: "Test Pit Excavation",
                                link: '/services/geotechnical_engineering/subsurface_exploration/soil_and_bedrock/test_pit'
                            }],
                        outerBlurb : 'Oftentimes, factors such as project budget, site access, subsurface composition, and/or drill rig availability, make geotechnical soil boring/coring methods impractical, inappropriate, and/or cost-prohibitive.  Therefore, we often employ mechanical excavating equipment to conduct subsurface explorations in the form of open test pit excavations. Excavation equipment is fairly commonplace (even in more remote areas), relatively inexpensive to operate, and tracked excavators (with skilled operators) can negotiate almost any type of terrain.  Therefore, mechanical excavators play an important role in geotechnical exploration. Test pit excavations can reveal a lot of visual information about the shallow subsurface that soil boring/coring cannot, including (but not limited to):<br/>'
                    }]
                }, {
                    urlPath: '/services/geotechnical_engineering/soil_sampling_methods',
                    pageTitle: 'Soil and Rock Sampling Methods',
                    indexBlurb: 'Our qualified exploration subcontractors offer a variety of subsurface exploration methods, equipment, and technologies. We select the most appropriate exploration method(s) to provide the most relevant subsurface information for a given project.',
                    _layout : 'aboutLayout',
                    subLinks : false


                }, {
                    _layout : 'aboutLayout',
                    urlPath: '/services/geotechnical_engineering/specialty_exploration_method',
                    pageTitle: 'Specialty Exploration Method Deployment',
                    indexBlurb: 'Alaska is known for its expansive and undeveloped landscapes, extreme weather, permafrost, and remote wilderness.  Unfortunately, it is these same qualities and features that can complicate geotechnical exploration; and it is often a challenge to mobilize and operate exploration equipment in these conditions. Over the years, we have worked with local exploration contractors to come up with inventive ways to operate in remote and harsh environments to obtain the subsurface information needed to properly support design and construction efforts.',
                    subLinkIntro: "<h4>Click on one of the links below to learn more about the different specialty subsurface exploration options available and their respective benefits, challenges, and limitations.</h4>",
                    subLinks: [{
                                            title: 'Land, water, and air deployment',
                                            link: '/services/geotechnical_engineering/specialty_exploration_method___land'
                                        }, {
                                            title: 'Refrigerated brine permafrost exploration and sampling',
                                            link: '/services/geotechnical_engineering/specialty_exploration_method___refrig'
                                        }, {
                                            title: 'Portable (aka backpack) rock core drilling',
                                            link: '/services/geotechnical_engineering/specialty_exploration_method___backpack'
                                }],

                    subPages: [{
                        urlPath: '/services/geotechnical_engineering/specialty_exploration_method___land',
                        pageTitle: 'Land, Air, Water Deploy'
                    }, {
                        urlPath: '/services/geotechnical_engineering/specialty_exploration_method___refrig',
                        pageTitle: 'Refrigerated Drilling'
                    }, {
                        urlPath: '/services/geotechnical_engineering/specialty_exploration_method___backpack',
                        pageTitle: 'Backpack Drill'
                    }]
                },
                {
                    urlPath: '/services/geotechnical_engineering/geophysical_exploration',
                    isSlideshow:true,
                    pageTitle: 'Geophysical Subsurface Exploration and Testing',
                    backLink: '/services/geotechnical_engineering',
                    subLinks: [{
                        title: 'Ground Penetrating Radar (GPR)',
                        link : '/services/geotechnical_engineering/geophysical_exploration___ground_penetrating_radar',
                    }, {
                        title: 'Electrical Resistivity',
                        link : '/services/geotechnical_engineering/geophysical_exploration___electrical_resistivity'
                    }, {
                        title: 'Downhole Seismic',
                        link : '/services/geotechnical_engineering/geophysical_exploration___downhole_seismic'
                    }, {
                        title: 'SPT Hammer Efficiency Analysis',
                        link : '/services/geotechnical_engineering/geophysical_exploration___spt'
                    }],
                    content: [{
                        title: "test1",
                        img: 'slides/geoexplore_1.jpg',
                        hideSlidesTitles : true,
                        hideSlideControls : true
                    }],
                    subPages: [{
                        urlPath: '/services/geotechnical_engineering/geophysical_exploration___ground_penetrating_radar',
                        isSlideshow: true,
                        backLink: '/services/geotechnical_engineering/geophysical_exploration',
                        pageTitle: 'Ground Penetrating Radar (GPR)',
                        subLinks: [{
                            title: 'Ground Penetrating Radar (GPR)',
                            link : '/services/geotechnical_engineering/geophysical_exploration___ground_penetrating_radar',
                        }, {
                            title: 'Electrical Resistivity',
                            link : '/services/geotechnical_engineering/geophysical_exploration___electrical_resistivity'
                        }, {
                            title: 'Downhole Seismic',
                            link : '/services/geotechnical_engineering/geophysical_exploration___downhole_seismic'
                        }, {
                            title: 'SPT Hammer Efficiency Analysis',
                            link : '/services/geotechnical_engineering/geophysical_exploration___spt'
                        }],
                        content: [{
                            title: "Ground penetrating radar 1",
                            img: 'slides/gpr_1.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'Ground penetrating radar 2',
                            img: 'slides/gpr_2.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'Ground penetrating radar 3',
                            img: 'slides/gpr_3.jpg',
                            hideSlidesTitles : true
                        }]
                    }, {
                        urlPath: '/services/geotechnical_engineering/geophysical_exploration___electrical_resistivity',
                        isSlideshow: true,
                        pageTitle: 'Electrical Resistivity',
                        subLinks: [{
                            title: 'Ground Penetrating Radar (GPR)',
                            link : '/services/geotechnical_engineering/geophysical_exploration___ground_penetrating_radar',
                        }, {
                            title: 'Electrical Resistivity',
                            link : '/services/geotechnical_engineering/geophysical_exploration___electrical_resistivity'
                        }, {
                            title: 'Downhole Seismic',
                            link : '/services/geotechnical_engineering/geophysical_exploration___downhole_seismic'
                        }, {
                            title: 'SPT Hammer Efficiency Analysis',
                            link : '/services/geotechnical_engineering/geophysical_exploration___spt'
                        }],
                        content: [{
                            title: "Electrical Resistivity 1",
                            img: 'slides/er_1.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'Electrical Resistivity 2',
                            img: 'slides/er_2.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'Electrical Resistivity 3',
                            img: 'slides/er_3.jpg',
                            hideSlidesTitles : true
                        }]
                    }, {
                        urlPath: '/services/geotechnical_engineering/geophysical_exploration___downhole_seismic',
                        isSlideshow: true,
                        pageTitle: 'Downhole Seismic',
                        subLinks: [{
                            title: 'Ground Penetrating Radar (GPR)',
                            link : '/services/geotechnical_engineering/geophysical_exploration___ground_penetrating_radar',
                        }, {
                            title: 'Electrical Resistivity',
                            link : '/services/geotechnical_engineering/geophysical_exploration___electrical_resistivity'
                        }, {
                            title: 'Downhole Seismic',
                            link : '/services/geotechnical_engineering/geophysical_exploration___downhole_seismic'
                        }, {
                            title: 'SPT Hammer Efficiency Analysis',
                            link : '/services/geotechnical_engineering/geophysical_exploration___spt'
                        }],
                        content: [{
                            title: "Downhole Seismic 1",
                            img: 'slides/downhole_1.jpg',
                            hideSlidesTitles : true
                        }]
                    }, {
                        urlPath: '/services/geotechnical_engineering/geophysical_exploration___spt',
                        isSlideshow: true,
                        pageTitle: 'SPT Hammer Efficiency Analysis',
                        subLinks: [{
                            title: 'Ground Penetrating Radar (GPR)',
                            link : '/services/geotechnical_engineering/geophysical_exploration___ground_penetrating_radar',
                        }, {
                            title: 'Electrical Resistivity',
                            link : '/services/geotechnical_engineering/geophysical_exploration___electrical_resistivity'
                        }, {
                            title: 'Downhole Seismic',
                            link : '/services/geotechnical_engineering/geophysical_exploration___downhole_seismic'
                        }, {
                            title: 'SPT Hammer Efficiency Analysis',
                            link : '/services/geotechnical_engineering/geophysical_exploration___spt'
                        }],
                        content: [{
                            title: "SPT 1",
                            img: 'slides/spt_1.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'SPT 2',
                            img: 'slides/spt_2.jpg',
                            hideSlidesTitles : true
                        }, {
                            title: 'SPT3 3',
                            img: 'slides/spt_3.jpg',
                            hideSlidesTitles : true
                        }]
                    }]
                }, {
                    urlPath: '/services/geotechnical_engineering/instrumentation',
                    _layout: 'aboutLayout',
                    pageTitle: 'Instrumentation and Monitoring',
                    backLink: '/services/geotechnical_engineering/subsurface_exploration',
                    subLinkIntro: '<h4>Subsurface instrumentation and monitoring are important facets of geotechnical exploration as it allows us to establish baseline measurements of subsurface conditions (e.g., groundwater table elevation, ground temperatures, etc.) and monitor how they change with time and as the result of various environmental and man-made factors.  We have extensive experience with various subsurface instrumentation equipment and procedures and are well-versed at developing monitoring programs geared to extract the most useful information from a given site for the least amount of disturbance, effort, and cost. <br/><br/>Click on one of the links below to learn more about the different instrumentation and monitoring services that we offer and their respective benefits, challenges, and limitations.</h4>',
                    subLinks: [{
                        title: 'Well/casing Installation',
                        link: '/services/geotechnical_engineering/instrumentation___well'
                    }, {
                        title: 'Groundwater Level Monitoring',
                        link: '/services/geotechnical_engineering/instrumentation___ground_water'
                    },
                    {
                        title: 'Pore Water Pressure Monitoring',
                        link: '/services/geotechnical_engineering/instrumentation___pore_water'
                    }, {
                        title: 'Ground Temperature Monitoring',
                        link: '/services/geotechnical_engineering/instrumentation___ground_water_temp'
                    }, {
                        title: 'Ground Settlement Monitoring',
                        link: '/services/geotechnical_engineering/instrumentation___ground_settlement'
                    }, {
                        title: 'Inclinometer Monitoring',
                        link : '/services/geotechnical_engineering/instrumentation___inclinometer'
                    }],
                    subPages: [{
                        urlPath: '/services/geotechnical_engineering/instrumentation___well',
                        pageTitle: 'Well/casing Installation'
                    }, {
                        urlPath: '/services/geotechnical_engineering/instrumentation___ground_water',
                        pageTitle: 'Groundwater Level Monitoring'
                    },
                    {
                        urlPath: '/services/geotechnical_engineering/instrumentation___pore_water',
                        pageTitle: 'Pore Water Pressure Monitoring'
                    },
                    {
                        urlPath: '/services/geotechnical_engineering/instrumentation___ground_water_temp',
                        pageTitle: 'Ground Temperature Monitoring'
                    }, {
                        urlPath: '/services/geotechnical_engineering/instrumentation___ground_settlement',
                        pageTitle: 'Ground Settlement Monitoring'
                    }, {
                        urlPath : '/services/geotechnical_engineering/instrumentation___inclinometer',
                        pageTitle: 'Inclinometer Monitoring'
                    }]
                }]
            }, {
                urlPath: '/services/geotechnical_engineering/surface_hydrology',
                pageTitle: 'Subsurface Exploration, Sampling and Monitoring',
                backLink: '/services/geotechnical_engineering/',
            }, {
                urlPath: '/services/geotechnical_engineering/computer_modeling',
                pageTitle: 'Computer Modeling and Analysis',
                backLink: '/services/geotechnical_engineering/'
            }, {
                urlPath: '/services/geotechnical_engineering/earthworks',
                pageTitle: 'Earthworks, Pavement Section Design, and Underground Utility Works',
                subLinks: [{
                            title: 'Cut and fill recommendations',
                            link: '/services/geotechnical_engineering/earthworks___cut'
                        }, {
                            title: 'Soil reinforcement',
                            link: '/services/geotechnical_engineering/earthworks___soil'
                        }, {
                            title: 'Slope stabilization and revetment',
                            link: '/services/geotechnical_engineering/earthworks___slope'
                        }, {
                            title: 'Retaining Wall Design',
                            link: '/services/geotechnical_engineering/earthworks___retain'
                        },{
                            title: 'Soil compaction and Ground Improvement',
                            link: '/services/geotechnical_engineering/earthworks___soil_compaction'
                        }, {
                            title: 'Mechanical and dynamic compaction',
                            link: '/services/geotechnical_engineering/earthworks___mech'
                        }, {
                            title: 'Structural pavement design',
                            link: '/services/geotechnical_engineering/earthworks___struct'
                        }, {
                            title: 'Trench shoring and protection',
                            link: '/services/geotechnical_engineering/earthworks___trench'
                }],
                subPages: [{
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___cut',
                        pageTitle: 'Cut and fill recommendations',
                        subLinks : false
                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___soil',
                        pageTitle: 'Soil reinforcement',
                        subLinks : false

                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___slope',
                        pageTitle: 'Slope stabilization and revetment',
                        subLinks : false

                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___retain',
                        pageTitle: 'Retaining Wall Design',
                        subLinks : false
                    },{
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___soil_compaction',
                        pageTitle: 'Soil compaction and Ground Improvement',
                        subLinks : false
                    },
                    {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___retain',
                        pageTitle: 'earthworks___retain',
                        subLinks : false                        
                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___retain',
                        pageTitle: 'Retaining Wall Design',
                        subLinks : false
                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___mech',
                        pageTitle: 'Mechanical and dynamic compaction',
                        subLinks : false
                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___struct',
                        pageTitle: 'Structural pavement design',
                        subLinks : false
                    }, {
                        _layout : "aboutLayout",
                        urlPath: '/services/geotechnical_engineering/earthworks___trench',
                        pageTitle: 'Trench shoring and protection',
                        subLinks : false
                    }

                ]
            },
            // missing photos
            {
                _layout : 'aboutLayout',
                urlPath: '/services/geotechnical_engineering/foundation_design',
                pageTitle: 'Foundation Design, Construction, Evaluation and Repair',
                header : 'servicesHeader',

                subLinks: [{
                            title: 'Conventional (shallow) concrete foundation design',
                            link: '/services/geotechnical_engineering/foundation_design___conventional',
                        }, {
                            title: 'Alternative (deep) Foundations',
                            link: '/services/geotechnical_engineering/foundation_design___alternative'
                        }, {
                            title: 'Foundation settlement assessment and repair',
                            link: '/services/geotechnical_engineering/foundation_design___settlement'
                        }, {
                            title: 'Frozen pile foundations',
                            link: '/services/geotechnical_engineering/foundation_design___frozen_pile'
                        }, {
                            title: 'Refrigerated Foundations',
                            link: '/services/geotechnical_engineering/foundation_design___refrig'
                        }, {
                            title: 'Tower Foundations and Rock-Soil Anchors',
                            link: '/services/geotechnical_engineering/foundation_design___tower'
                        }, {
                            title: 'Bridge foundation and scour analysis',
                            link: '/services/geotechnical_engineering/foundation_design___bridge'
                        }, {
                            title: 'Pile load testing',
                            link: '/services/geotechnical_engineering/foundation_design___pile'
                }],
                subPages: [{
                    urlPath: '/services/geotechnical_engineering/foundation_design___conventional',
                    pageTitle: 'Conventional (shallow) concrete foundation design',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___alternative',
                    pageTitle: 'Alternative (deep) Foundations',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___settlement',
                    pageTitle: 'Foundation settlement assessment and repair',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___frozen_pile',
                    pageTitle: 'Frozen pile foundations',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___refrig',
                    pageTitle: 'Refrigerated Foundations',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___tower',
                    pageTitle: 'Tower Foundations and Rock-Soil Anchors',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___bridge',
                    pageTitle: 'Bridge foundation and scour analysis',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/foundation_design___pile',
                    pageTitle: 'Pile load testing',
                    subLinks : false
                }]
            }, {
                _layout : 'aboutLayout',
                urlPath: '/services/geotechnical_engineering/dam_inspection',
                pageTitle: 'Dam Inspection, Design, And Construction',
                header : 'servicesHeader',
                subPages: [{
                    urlPath: '/services/geotechnical_engineering/dam_inspection___inspection',
                    pageTitle: 'Dam Inspection',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/dam_inspection___design',
                    pageTitle: 'Dam Design',
                    subLinks : false
                }, {
                    urlPath: '/services/geotechnical_engineering/dam_inspection___construct',
                    pageTitle: 'Dam Constructon Coordination',
                    subLinks : false
                }],
                'subLinks': [{
                    title: 'Dam Inspection',
                    link: '/services/geotechnical_engineering/dam_inspection___inspection',
                }, {
                    title: 'Dam Design',
                    link: '/services/geotechnical_engineering/dam_inspection___design',
                }, {
                    title: 'Dam Constructon Coordination',
                    link: '/services/geotechnical_engineering/dam_inspection___construct',
                }]
            }, {
                urlPath: '/services/geotechnical_engineering/waterfront_design',
                pageTitle: 'Waterfront Design and Construction',
                subLinks: [{
                    title: 'Sheet pile docks',
                    link: '/services/geotechnical_engineering/waterfront_design___sheet'
                }, {
                    title: 'Mooring dolphins',
                    link: '/services/geotechnical_engineering/waterfront_design___mooring'
                }],
                subPages: [{
                    _layout : 'aboutLayout',
                    urlPath: '/services/geotechnical_engineering/waterfront_design___sheet',
                    pageTitle: 'Sheet pile docks',
                    subLinks : false
                }, {
                    _layout : 'aboutLayout',
                    urlPath: '/services/geotechnical_engineering/waterfront_design___mooring',
                    pageTitle: 'Mooring dolphins',
                    subLinks : false
                }]
            }, {
                urlPath: '/services/geotechnical_engineering/management',
                pageTitle: 'Construction Management'
            }, {
                urlPath: '/services/geotechnical_engineering/other',
                pageTitle: 'Other Services',
                // subLinks : [
                //     {
                //         title: 'Quarry Borrow Source Evaluations',
                //         //link: '/services/geotechnical_engineering/waterfront_design___sheet'
                //     }
                // ]
            }
        ]
    }, {
        urlPath: '/services/construction_site_materials_testing',
        pageTitle: 'Construction Site Materials Testing and Observation'
    }, {
        // kind of funky not using normal stuff ...
        _layout : 'aboutLayout',
        urlPath: '/services/icc_special_inspection',
        pageTitle: 'ICC Special Inspection',
        main: 'services_icc_special_inspection',
        keywords : [
                    'Post-tension',
                    'Bolting',
                    'Welding',
                    'Fire-proofing',
                    'Rebar',
                    'Anchors'
        ],
        subLinkIntro : 'The ICC Board of Directors may take any actions it deems necessary in order to enforce this Code of Ethics and to preserve the integrity of the International Code Council.',
        subPages: [{
            urlPath: "/services/icc_special_inspection/concrete",
            // fix this...
            main : "services_icc_special_inspection__concrete",
            pageTitle: 'Concrete Placement'
        }, {
            urlPath: '/services/icc_special_inspection/concrete_reinforcement',
            main : "services_icc_special_inspection__concrete_reinforcement",
            pageTitle: 'Concrete Reinforcement'
        }, {
            urlPath: "/services/icc_special_inspection/dica",
            main : "services_icc_special_inspection__dica",
            pageTitle: 'Drilled In Concrete Anchors (DICA)'
        }, {
            urlPath: '/services/icc_special_inspection/fireproofing',
            main : "services_icc_special_inspection__fireproofing",
            pageTitle: 'Fire Proofing Inspection'
        },{
            urlPath: '/services/icc_special_inspection/bolting',
            main : "services_icc_special_inspection__bolting",
            pageTitle: 'High Strength Bolting Inspection'
        }, {
            urlPath: '/services/icc_special_inspection/masonry',
            main : "services_icc_special_inspection__masonry",
            pageTitle: 'Masonry Inspection (CMU-concrete masonry unit)'
        }, {
            urlPath: '/services/icc_special_inspection/pre_post_tensioning',
            main : "services_icc_special_inspection__pre_post_tensioning",
            pageTitle: 'Pre-Stressed Post Tensioning Concrete'
        }, {
            urlPath: '/services/icc_special_inspection/welding',
            main : "services_icc_special_inspection__welding",
            pageTitle: 'Welding Inspection'
        }, {
            urlPath: '/services/icc_special_inspection/structural',
            main : "services_icc_special_inspection__structural",
            pageTitle: 'Structural Wood Framing'
        }, {
            urlPath: '/services/icc_special_inspection/soils',
            main : "services_icc_special_inspection__soils",
            pageTitle: 'Soils Inspection'
        }]
    }, {
        _layout : 'aboutLayout',
        header : 'servicesHeader',
        urlPath: '/services/lab_materials_testing',
        pageTitle: 'Lab Materials Testing',
        keywords : [
                    'Soils',
                    'Aggregate',
                    'Concrete',
                    'Asphalt',
                    'Triaxial',
                    'Consolidation',
                    'Permeability',
                    'Proctor',
                    'Density',
                    'Frost Class',
                    'Soundness',
                    'Durability',
                    'Degradation',
                    'Gradation',
                    'Freeze',
                    'Thaw',
                    'Sieve',
                    'Marshall',
                    'AASHTO',
                    'CCRL',
                    'WAQTC',
                    'AMRL',
                    'Riprap',
                    'Armor',
                    'Specific Gravity'
        ],
        subPages: [{
            urlPath: '/services/lab_materials_testing/construction_materials_testing',
            main : 'services_lab_materials_testing__construction_materials_testing',
            pageTitle: 'Construction Materials Laboratory Testing',
            subLinks : false
        }, {
            urlPath: '/services/lab_materials_testing/specialty_lab_testing',
            main : 'services_lab_materials_testing__specialty_lab_testing',
            pageTitle: 'Specialty Geotechnical Laboratory Testing',
            subLinks : false
        }, {
            urlPath: '/services/lab_materials_testing/other',
            main : 'services_lab_materials_testing__other',
            pageTitle: 'Other Laboratory Services',
            subLinks : false
        }]
    }]
};
var subPageFilterList = function(subPages){
    result = [];
    if(typeof subPage.subPages != "undefined"){
        result.push(subPageFilterList(subPages.subPages));
    }
    return result;
}
Meteor.startup(function(){
//    Context.remove({});
    if(Context.find().fetch().length === 0){
        // insert context

        for(var key in Contents){
            Contents[key].filter(function(a,i){
//                Contents[key]
                if(key == 'people'){
                    console.log(a);
                }
                if(typeof a.subPages != "undefined"){
                    if(key == 'people'){
                        console.log("people has sub pages");
                    }
                    a.subPages.filter(function(x){
                        if(key == 'people'){
                            console.log(x);
                        }   
                        if(typeof x.subPages != "undefined"){
                            x.subPages.filter(function(x2){
                                //console.log(x2);
                                if(typeof x2.subPages != "undefined"){
                                    console.log('x2 has sub pages');
                                    x2.subPages.filter(function(x3){
                                        if(typeof x3.subPages != "undefined"){
                                            x3.subPages.filter(function(x4){
                                                console.log(x4);
                                                if(typeof x4.subPages != "undefined"){
                                                    x4.subPages.filter(function(x5){
                                                        console.log(x5);
                                                        if(typeof x5.subLinks == "undefined" && typeof x4.subLinks != "undefined"){
                                                            x5.subLinks = x4.subLinks;
                                                        }
                                                        console.log("******\t " +x5.urlPath + Context.insert(x5));
                                                    });
                                                    delete x4.subPages;
                                                    if(typeof x4.subLinks == 'undefined' && typeof x3.subLinks != "undefined"){
                                                        x4.subLinks = x3.subLinks;
                                                    }
                                                }
                                                console.log("*****\t " +x4.urlPath + Context.insert(x4));
                                            });
                                            delete x3.subPages;
                                            if(typeof x3.subLinks == "undefined" && typeof x2.subLinks != "undefined"){
                                                x3.subLinks = x2.subLinks;
                                            }
                                        }
                                        if(typeof x3._layout == "undefined" && typeof x2._layout != "undefined"){
                                            x3._layout = x2._layout;
                                        }
                                        console.log("****\t" +x3.urlPath + Context.insert(x3));
                                    });
                                    delete x2.subPages;
                                    if(typeof x2.subLinks == "undefined" && typeof x.subLinks != "undefined"){
                                        x2.subLinks = x.subLinks;
                                    }
                                }
                                if(typeof x2._layout == "undefined" && typeof x._layout != "undefined"){
                                    x2._layout = x._layout;
                                }
                                if(typeof x2.header == "undefined" && typeof x.header != "undefined"){
                                    x2.header = x.header;
                                }else if(typeof x2.header == "undefined" && typeof a.header != "undefined"){
                                    x2.header = a.header;
                                }
                                if(typeof x2.subLinks == "undefined" && typeof x.subLinks != "undefined"){
                                    x2.subLinks = x.subLinks;
                                }
                                console.log("***\t" +x2.urlPath + Context.insert(x2));
                            });
                            delete x.subPages;
                            if(typeof x.subLinks == "undefined" && typeof a.subLinks != "undefined"){
                                x.subLinks = a.subLinks;
                            }
                        }
                        if(typeof x._layout == "undefined" && typeof a._layout != "undefined"){
                            x._layout = a._layout;
                        }
                        if(typeof x.header == "undefined" && typeof a.header != "undefined"){
                            x.header = a.header;
                        }
                        if(typeof x.subLinks == "undefined" && typeof a.subLinks != "undefined"){
                            x.subLinks = a.subLinks;
                        }
                        console.log("**\t" +x.urlPath + Context.insert(x));

                    });
                    delete a.subPages;
                }

                console.log("*\t" +a.urlPath + Context.insert(a));

            //console.log("**\t inserting : +" + key + "+\t" + Context.insert(a));



            })
            if(typeof Contents[key].subPages != "undefined"){
                console.log('here they are!');
            }


        }
        // run index stuff?
        
    }
});

Meteor.publish('getUrl',function(urlPath){
    if(typeof urlPath == "string" && urlPath.trim() != ''){
        console.log(urlPath);
        return Context.find({urlPath : urlPath.trim()});
    }
    return false;
})