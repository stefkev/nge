//http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

RegExp.escape = function(s) {  
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};

Meteor.methods({
	'getPathData' : function(urlPath){
		return getPathData(urlPath);
	},
	'searchIndex' : function(term){
		return Context.find( { pageTitle: { $regex: RegExp.escape(term), $options: 'i' }  } ).fetch();
	},
	'setDocumentOwner' : function(docId,userId){
		if(this.userId && Roles.userHasRole(this.userId,'admin')){
			console.log('setting doc ' + docId + ' to owner : ' + userId);
			return Documents.update({_id : docId},{"$set" : {owner : userId } });
		}else{
			console.log("did not have permission");
		}
	}

});


Meteor.startup(function(){

	expandPath = function(term){
	    var r = term.split('/'),result = [],path = '';
	    r.shift();
	    r.filter(function(o,i){
	        var words = [];
	        path += o + '/';
	        if(i>0 && i < 3){
	            o.split('_').filter(function(x,n){

	                words.push(x.toProperCase()) ;
	            })
	            result.push({
	                path: path,
	                text : words.join(' ')
	            });
	        }else{
	        	;
	        }
	    });
	    if(result && result.length > 0){
	        return result;
	    }
	    return false;
	};

	
})