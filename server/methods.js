process.env.MAIL_URL = 'smtp://inquiry@bbsalaska.com:buzzbizz2899@smtp.gmail.com:587';


Meteor.methods({
	'grantAdminAccess' : function(secret){
		// 'look up admin usernam'
		if(typeof secret == "undefined" || secret != "passphrase"){
			return false;
		}
		if(typeof Roles == "undefined"){
			return false;
		}
		var u =Meteor.users.findOne({username:"admin"});
		if(u){
			if (!Roles.addUserToRoles(u._id, 'admin')) {
				console.log("Error with roles");
			}
		}
	},
    'createDocumentUser': function(email, pw,username) {
        if (this.userId && Roles.userHasRole(this.userId, 'admin')) {
            if (typeof email == "string" && typeof pw == "string") {
            	if(typeof username != "undefined" && username != ''){
	            	return Accounts.createUser({
	                    email: email,
	                    username : username,
	                    password: pw
	                });
            	}
                return Accounts.createUser({
                    email: email,
                    password: pw
                });
            }
        } else {
            return false;
        }

    },
    'removeDocumentUser': function(uid){
    	if(typeof uid == "undefined"){
    		return false;
    	}
    	if (this.userId && Roles.userHasRole(this.userId, 'admin')) {
            return Meteor.users.remove({_id : uid});
        } else {
            return false;
        }
    },
    sendEmailDocumentLink : function(uid,docId){
    	// use a template?
    	if (this.userId && Roles.userHasRole(this.userId, 'admin') && typeof uid != "undefined" && typeof docId != "undefined") {
    		var user = Meteor.users.findOne(uid);
    		// get email
    		console.log(user);
    		if(user.emails != "undefined" && user.emails.length == 1){
    			if(typeof user.emails[0].address != "undefined"){
    				var email = user.emails[0].address;
    			}
    		}
    	}else{
    		return false;
    	}
    	var link = Documents.findOne({_id: docId,owner : uid}).url();
    	if(!link || typeof link == "undefined" || link == ''){
    		// get rid of leading slash
    		link.shift();
    		console.log('Document : ' + docId + ' does not have a url for user ' + uid);
    		return false;
    	}
    	var message = 'Northern Geotechnical has shared a document. Please log-in before viewing. ' + process.env.ROOT_URL + link;
        this.unblock();
        var e = Email.send({
            to: email,
            from: 'doNotReply@bbsalaska.com',
            subject: "Northen Geotechical - Shared Document available for viewing",
            text: message
        });
        if(e){
        	Meteor.call('markEmailSent',uid,docId);
        	// update document to let it know a reminder email was sent
        	return e;
        }else{
			console.log("Could not send email");
			return false;
		}
    },
	'markEmailSent' : function(userId,docId){
		console.log(this.userId);
		var r =  Documents.update({_id : docId,owner:userId},{"$set" : {emailSent : new Date() } });
		if(r){
			return r;
		}else{
			return false;
		}
		
	},
    'getBreadCrumb' : function(path){
        // sending the url via the method does some weird things to it
        console.log(typeof path);
        path = path + '';
        path = path.split(',');
        console.log(path);
        // will need to check for subpages if length == 2?
        if(path.length == 1 || path.length == 2){
            path = path[0];
            if(typeof Contents[path] != "undefined"){
                console.log("has path");
                console.log(Contents[path].subPages);
                var r = [];
                Contents[path].filter(function(a,i){
                    console.log(a);
                    if(a.urlPath == '/'+path[0] && typeof a.subLinks != "undefined"){
                        r = subLinks;
                    }
                });
                return r;
            }
        }else if(path.length == 2){
            if(typeof Contents[path[0]][path[1]] != "undefined"){
                console.log ("returning two levels - maybe?")
                return Contents[path[0]][path[1]].subLinks;
            }
        }
        return false;
    }
});

