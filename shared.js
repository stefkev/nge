import {
    Meteor
}
from 'meteor/meteor';
import { Mongo } from 'meteor/mongo'
Context = new Mongo.Collection("nge");
if(typeof FS != "undefined"){
  var imageStore = new FS.Store.GridFS('images');

  Documents = new FS.Collection('documents', {
   stores: [imageStore]
  });


  Documents.allow({
   insert: function(userId){
   var u = (typeof userId != "undefined" ? userId : (typeof this.userId != "undefined" ? this.userId : false));
   console.log(u);
    if(u && Roles.userHasRole(u,'admin'))
     return true;
    else
     return false;
   },
   update: function(userId){
    var u = (typeof userId != "undefined" ? userId : (typeof this.userId != "undefined" ? this.userId : false));
    if(u && Roles.userHasRole(u,'admin'))
     return true;
    else
     return false;
   },
   remove: function(userId){
    var u = (typeof userId != "undefined" ? userId : (typeof this.userId != "undefined" ? this.userId : false));
    if(u && Roles.userHasRole(u,'admin'))
     return true;
    else
     return false;
   },
   download: function(userId,doc){
    var u = (typeof userId != "undefined" ? userId : (typeof this.userId != "undefined" ? this.userId : false));

    //console.log(Meteor.user());
    //console.log(doc);
    if(typeof u != "undefined" && u){
      // defaults to documents being available to all logged in users everyone if an owner is not set
      if(typeof doc.owner != "undefined" && doc.owner != u && !Roles.userHasRole(u,'admin') ){
        return false;
      }
      return true;
    }
    return false;  
   }
  });
}else{
  console.log("CollectionFS Not installed");
}

Meteor.users.allow({
  insert: function (userId, doc) {
    var u = (typeof userId != "undefined" ? userId : (typeof this.userId != "undefined" ? this.userId : false));
    // only admin can insert 
    if(u && Roles.userHasRole(this.userId, 'admin')){ 
      return true;
    }
    return false;
  },
  update: function (userId, doc, fields, modifier) {
    console.log("user "+userId+"wants to modify doc"+doc._id);
    if (userId && doc._id === userId) {
      console.log("user allowed to modify own account!");
      // user can modify own 
      return true;
    }
    // admin can modify any
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  },
  remove: function (userId, doc) {
    // only admin can remove
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  }
});