

Template.contact.helpers({
    'isEmail' : function(){
        console.log(this);
        if(typeof this.type != "undefined"){
            return (this.type == "E-mail" ? true : false);
        }
        return false;
    },
    'contacts': function() {
        return [{
            group: 'Engineering',
            data: [{
                subTitle: "General Inquires and Engineering Support",
                contact: "Keith Mobley, P.E. – President, Senior Geotechnical Engineer",
                contactInfo: [{
                    type: 'Direct',
                    data: '(907) 771-9506'
                }, {
                    type: 'Cell',
                    data: '(907) 529-9180'
                }, {
                     type: 'Fax',
                     data: '(907) 344-5993'
                }, {
                    type: 'E-mail',
                    data: 'kmobley@nge-tft.com'
                }]},
                {
                    subTitle: "Fee Proposals and Exploration Scheduling",
                    contact: "Andrew Smith, CPG – Senior Geologist",
                    contactInfo: [{
                        type: 'Direct',
                        data: '(907) 771-9507'
                    }, {
                        type: 'Cell',
                        data: '(907) 727-5636'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'asmith@nge-tft.com'
                    }]
                }
            ]
            },
            {
                group: 'Field Testing and Inspection',
                data:[{
                    subTitle: "General Inquires, Project Bidding, and Technical Questions",
                    contact: "Ron Caron – Vice President, Construction Site Materials Testing and Observation Director ",
                    contactInfo: [{
                        type: 'Direct',
                        data: '(907) 771-9509'
                    }, {
                        type: 'Cell',
                        data: '(907) 529-1040'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'rcaron@nge-tft.com'
                    }]
                },{
                    subTitle: "Field Scheduling / Testing/Inspection Scheduling",
                    contact: "Mark Dean – Construction Site Materials Testing and Observation/SI Field Manager",
                    contactInfo: [{
                        type: 'Direct',
                        data: '(907) 771-9504'
                    }, {
                        type: 'Cell',
                        data: '(907) 529-2805'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'mdean@nge-tft.com'
                    }]
                },{
                    subTitle: "General Inquires, Project Bidding, and Technical Questions",
                    contact: "Norman \“Brent\” Martin – Senior Special Inspector",
                    contactInfo: [{
                        type: 'Direct',
                        data: '(907) 771-9509'
                    }, {
                        type: 'Cell',
                        data: '(907) 529-1040'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'rcaron@nge-tft.com'
                    }]
                }
                ]
            },
            {
                group: 'Laboratory Testing',
                data: [{
                    subTitle: "General Inquires, Pricing, and Scheduling",
                    contact: "Andrew Fortt – Laboratory Manager",
                    contactInfo: [{
                        type: 'Direct',
                        data: '(907) 771-9503'
                    }, {
                        type: 'Cell',
                        data: '(907) 782-7280'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'afortt@nge-tft.com'
                    }]
                }]
            },
            {
                group: 'Administrative',
                data: [{
                    subTitle: "Accounting",
                    contact: "Joy Gray - Accounts Billable/Payable",
                    contactInfo: [{
                        type: 'Office',
                        data: '(907) 344-5934'
                    }, {
                        type: 'Direct',
                        data: '(907) 771-9501'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'jgray@nge-tft.com'
                    }]
                },{
                    subTitle: "Human Resources",
                    contact: "Joy Gray – Human Resources Administrator",
                    contactInfo: [{
                        type: 'Office',
                        data: '(907) 344-5934'
                    }, {
                        type: 'Direct',
                        data: '(907) 771-9501'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'jgray@nge-tft.com'
                    }]
                },{
                    department: "Safety",
                    contact: "Andrew Fortt – Safety Director",
                    contactInfo: [{
                        type: 'Direct',
                        data: '(907) 771-9503'
                    }, {
                        type: 'Cell',
                        data: '(907) 782-7280'
                    }, {
                        type: 'Fax',
                        data: '(907) 344-5993'
                    }, {
                        type: 'E-mail',
                        data: 'afortt@nge-tft.com'
                    }]
                }]
            }
        ];
    //     return [{
    //             department: "GEOTECHNICAL ENGINEERING",
    //             subTitle: "General Inquires and Engineering Support",
    //             contact: "Keith Mobley, P.E. – President, Senior Geotechnical Engineer",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9506'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 529-9180'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'kmobley@nge-tft.com'
    //             }]
    //         }, {
    //             department: "Geotechnical",
    //             subTitle: "Fee Proposals and Exploration Scheduling",
    //             contact: "Andrew Smith, CPG – Senior Geologist",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9507'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 727-5636'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'asmith@nge-tft.com'
    //             }]
    //         }, {
    //             department: "Construction Site Materials Testing and Observation",
    //             subTitle: "General Inquires, Project Bidding, and Technical Questions",
    //             contact: "Ron Caron – Vice President, Construction Site Materials Testing and Observation Director ",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9509'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 529-1040'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'rcaron@nge-tft.com'
    //             }]
    //         }, {
    //             department: "Field Scheduling / Testing/Inspection Scheduling",
    //             contact: "Mark Dean – Construction Site Materials Testing and Observation/SI Field Manager",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9504'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 529-2805'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'mdean@nge-tft.com'
    //             }]
    //         }, {
    //             department: "SPECIAL INSPECTION",
    //             subTitle: "General Inquires, Project Bidding, and Technical Questions",
    //             contact: "Norman \“Brent\” Martin – Senior Special Inspector",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9509'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 529-1040'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'rcaron@nge-tft.com'
    //             }]
    //         }, {
    //             department: "LABORATORY TESTING",
    //             subTitle: "General Inquires, Pricing, and Scheduling",
    //             contact: "Andrew Fortt – Laboratory Manager",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9503'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 782-7280'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'afortt@nge-tft.com'
    //             }]
    //         }, {
    //             department: "ACCOUNTING",
    //             contact: "Joy Gray - Accounts Billable/Payable",
    //             contactInfo: [{
    //                 type: 'Office',
    //                 data: '(907) 344-5934'
    //             }, {
    //                 type: 'Direct',
    //                 data: '(907) 771-9501'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'jgray@nge-tft.com'
    //             }]
    //         }, {
    //             department: "HUMAN RESOURCES",
    //             contact: "Joy Gray – Human Resources Administrator",
    //             contactInfo: [{
    //                 type: 'Office',
    //                 data: '(907) 344-5934'
    //             }, {
    //                 type: 'Direct',
    //                 data: '(907) 771-9501'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'jgray@nge-tft.com'
    //             }]
    //         }, {
    //             department: "SAFETY",
    //             subTitle: "General Inquires",
    //             contact: "Andrew Fortt – Safety Director",
    //             contactInfo: [{
    //                 type: 'Direct',
    //                 data: '(907) 771-9503'
    //             }, {
    //                 type: 'Cell',
    //                 data: '(907) 782-7280'
    //             }, {
    //                 type: 'Fax',
    //                 data: '(907) 344-5993'
    //             }, {
    //                 type: 'E-mail',
    //                 data: 'afortt@nge-tft.com'
    //             }]
    //         }
    //
    //     ];
    }
});