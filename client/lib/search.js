Template.navRows.onCreated(function(){
	Session.set("searchTerm",undefined);
	Session.set("searchResult",undefined);
});
Template.navRows.events({
	'click .search' : function(evt,tmpl){
		var term = tmpl.find('#search_term');
		console.log(term.value);
		if(term && term.value != ''){
			Session.set("searchTerm",term.value);

			Meteor.call('searchIndex',Session.get("searchTerm"),function(e,r){
				if(typeof e == "undefined" || typeof e == null){
					console.log(r);
					Session.set("searchResult",_.flatten(r));
				}
			});
			//window.location.pathname = decodeURIComponent("/search?term=" + term.value);
		}else{
			Session.set("searchTerm",undefined);
		}
		
		console.log('clickedSearch');
	},
})

Template.searchModal.onRendered(function(){
	//$('#searchModal').modal({ show: false})
	if(!Session.equals("searchTerm",undefined)){
		console.log();
	}
});

Template.searchModal.helpers({
	'matching' : function(){
		if(Session.equals('searchResult'),undefined){
			return false;
		}else{
			return Session.get("searchResult");

		}
	},
	'searchTerm' : function(){
		return Session.get("searchTerm");	
	}
})

Template.searchModal.events({
	'click .searchLink' : function(){
		console.log("clicked li a");
		console.log(this);
		window.location.href = this.url;
	},
	'click .topicLink' : function(){
		console.log("clicked li a");
		console.log(this);
		window.location.href = this.path;
	}
})