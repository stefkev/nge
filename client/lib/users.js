Template.users.onCreated(function() {

    var self = this;
    this.autorun(function() {
        self.subscribe("users");
        //self.subscribe("documents");
    });
});

Template.users.helpers({
    'getUsers': function() {
        return Meteor.users.find().fetch();
    },
    'isAdmin': function() {
        return Roles.userHasRole(Meteor.userId(), 'admin');
    }

});

Template.users.events({
    'click .remove': function() {
        if (Roles.userHasRole(Meteor.userId(), 'admin')) {
        	if(this._id == Meteor.userId()){
        		toastr.error("You cannot remove your own account.");
        		return false;
        	}
            Meteor.call("removeDocumentUser", this._id, function(e, r) {
                if (typeof e != 'undefined') {
                    toastr.error("Error " + e);
                } else {
                    toastr.success("User removed");
                }
            })
        } else {
            toastr.error("Access denied");
        }
    },
    'click #makeUser': function(evt, tmpl) {
        var email = tmpl.find('#email');
        var pw = tmpl.find('#pwd');
        var pw_c = tmpl.find('#pwd_c');

        if (email && pw && pw_c) {
            if (email.value != '') {
                if (pw.value != '' && pw_c.value != '') {
                    if (pw.value != pw_c.value) {
                        toastr.error("Password confirmation value is not the same");
                        return false;
                    }
                } else {
                    toastr.error("Missing password information");
                    return false;
                }
            } else {
                toastr.error("Missing email address");
                return false;
            }
        } else {
            toastr.error("Missing email address");
            return false;
        }
        var username = tmpl.find('#username');
        if(username && typeof username.value != "undefined" && username.value != ''){
        	Meteor.call('createDocumentUser', email.value, pw.value, username.value, function(e, r) {
        		if(r){
        			toastr.success("User Created");
        		}else if (typeof e != 'undefined' || typeof e != null) {
	                toastr.error("Error " + e);
	            } 
	        });
        }else{
			Meteor.call('createDocumentUser', email.value, pw.value, function(e, r) {
	            if (typeof e != 'undefined' || typeof e != null) {
	                toastr.error("Error " + e);
	            } else {
	                toastr.success("User Created");
	            }
	        });
		}
        
    }
})