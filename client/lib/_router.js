import { Mongo } from 'meteor/mongo'

Template.loginButtons.rendered = function()
{  
    // if not logged in ... 
    Deps.autorun(function(){
        if(!Meteor.user()){
            Accounts._loginButtonsSession.set('dropdownVisible', true);
        }else{
            Accounts._loginButtonsSession.set('dropdownVisible', false);
        }
    });
    
};
Template.breadcrumbNav.onCreated(function(){
    var path = location.pathname.split('/');
    Session.set('breadcrumb',undefined);
    console.log(path);
    path.shift();
    console.log(path);
    
    Meteor.call("getBreadCrumb",path,function(e,r){
        if(typeof e != "undefined"){
            console.log("error with breadcrumb call");
        }else{
            console.log(r);
            Session.set("breadcrumb",r);
        }
    });
});

Template.breadcrumbNav.helpers({
    'isCurrentPage' : function(){
        console.log(this);
        location.pathname;
    },
    'getCrumbs' : function(){
        console.log(this);
        var crumb = Session.get("breadcrumb");

        if(crumb){
            // do selected page based on location?
            return crumb;
        }

    }
})

FlowRouter.route('/', {
    action: function(params, queryParams) {
        BlazeLayout.render("layout", {
            main: "index"
        });
        return true;
    }
});
var buildBlazeLayout = function(obj,params){
    console.log(params);
    var blazeObj = Context.findOne({urlPath:location.pathname});
    console.log(blazeObj);
    console.log(location.pathname);
    if(typeof blazeObj == "undefined" || !blazeObj){
        return false;
    }
    if(typeof blazeObj.bodyBg == "undefined"){
        blazeObj.bodyBg ='/img/about_bg_b.svg' ;
    }
    if(typeof blazeObj.showcaseBg == "undefined"){
        blazeObj.showcaseBg = '/img/' + 
        params.pageName + 
        (typeof params.subPageName != "undefined" ? '_' + params.subPageName + 
            (typeof params.innerPageName != 'undefined' ?   '__' + params.innerPageName : '' ) 
        : '' ) 
        + '_showcase.jpg';
    }
    console.log(blazeObj.showcaseBg);
    if(blazeObj){
        // this needs better check... 
        if(typeof obj == "object"){
            for(var k in obj){
                blazeObj[k] = obj;
            }
        }

        if(typeof blazeObj.footer == "undefined"){
            blazeObj.footer = 'footer';
        }
        return blazeObj;
    }
    return false;
}
FlowRouter.wait();
Meteor.subscribe('getUrl',location.pathname,function(){
            FlowRouter.initialize();
        });
FlowRouter.route('/:pageName/:subPageName/:innerPageName/:section/:finalPage', {
    action: function(params, queryParams) {
        //var context = Context.findOne({urlPath:location.pathname});
        // if nothing then reload again?
        var blazeObj = buildBlazeLayout(
                        { showcaseBg : '/img/' + params.pageName + '_' 
                                              + params.subPageName + '__' 
                                              + params.innerPageName + '_showcase.jpg'},params);


        if(blazeObj){
            console.log(blazeObj);
        //blazeObj.main = params.pageName + '_' + params.subPageName + '__' + params.innerPageName;
            if (blazeObj.urlPath == '/services/geotechnical_engineering/soil_sampling_methods'){
                console.log('it is innerpage')
                blazeObj.titleCaption = 'Our qualified exploration subcontractors offer a variety of subsurface exploration methods, ' +
                    'equipment, and technologies. We select the most appropriate exploration method(s) to provide the most relevant subsurface' +
                    ' information for a given project.';
            }
            if(typeof blazeObj.header == "undefined"){
                blazeObj.header = 'servicesHeader';
            }
            if (typeof blazeObj.showcaseBg != "undefined") {
                $("#about_background").backstretch(blazeObj.showcaseBg);
            }
            if(typeof blazeObj._layout == "undefined"){
                blazeObj._layout = 'projectsLayout';
            }
            return BlazeLayout.render(blazeObj._layout, blazeObj);
        }else{
            return false;
        }
    }
});

FlowRouter.route('/cfs/files/documents', {
    action: function(params, queryParams) {
        console.log("HERE");
    }
});

FlowRouter.route('/:pageName/:subPageName/:innerPageName', {
    action: function(params, queryParams) {
        console.log('here');
        var blazeObj = buildBlazeLayout({},params);

        if(blazeObj){
            if(typeof blazeObj.header == "undefined"){
                blazeObj.header = 'servicesHeader';
            }

            if (typeof blazeObj.showcaseBg != "undefined") {

                $("#about_background").backstretch(blazeObj.showcaseBg);
            }
                                console.log("showcase is not undefined");
                                console.log(blazeObj);

            if(typeof blazeObj.footer == "undefined"){
                blazeObj.footer = 'footer';
            }
            if(typeof  blazeObj.main== "undefined"){
                blazeObj.main = params.pageName + '_' + params.subPageName + '__' + params.innerPageName;
            }
            if(typeof blazeObj._layout == "undefined"){
                blazeObj._layout = 'projectsLayout';
            }
            console.log(blazeObj);
            return BlazeLayout.render(blazeObj._layout, blazeObj);
        }else{
            console.log("no path to url");
            return false;
        }
    }      
});
// to do!
FlowRouter.route('/:pageName/:subPageName', {
    
    action: function(params, queryParams) {
        if (params.pageName != '') {
            var blazeObj = Context.findOne({urlPath:location.pathname});
            if(blazeObj){
                console.log("LETS GO!");
            }
            if (params.pageName == "about") {
                if (params.subPageName != '') {
                    console.log('/img/' + params.pageName + '_' + params.subPageName + '_showcase.jpg');
                    var pageTitle = '';
                    console.log(params.subPageName)
                    if (params.subPageName == 'company_bio') {
                        pageTitle = 'The Northern Geotechnical Engineering- Terra Firma Testing Story:';
                    } else if (params.subPageName == 'mission') {
                        pageTitle = 'The Northern Geotechnical Engineering- Terra Firma Testing Mission Statement';
                    } else if (params.subPageName == 'code_of_ethics') {
                        pageTitle = 'Engineer and Engineering Technician Code of Ethics';
                    } else if (params.subPageName == 'news') {
                        pageTitle = 'News and Upcoming Events';
                    } else if (params.subPageName == 'pro_affiliations') {
                        pageTitle = 'Professional Affiliations';
                    } else if (params.subPageName == 'client_reviews') {
                        pageTitle = 'Client Endorsements';
                    }
                    return BlazeLayout.render("aboutLayout", {
                        header: 'aboutHeader',
                        main: params.pageName + '_' + params.subPageName,
                        pageTitle: pageTitle,

                        footer: 'footer',
                        bodyBg: '/img/about_bg_b.svg',
                        showcaseBg: '/img/' + params.pageName + '_' + params.subPageName + '_showcase.jpg',
                        //showcaseBg : 'about_bg_' + params.subPageName

                    });
                } else {


                    return BlazeLayout.render("aboutLayout", {
                        header: 'aboutHeader',
                        main: params.pageName,
                        bodyBg: '/img/about_bg_b.svg',
                        footer: 'footer'
                    });
                }
            } else if (params.pageName == "services") {
                console.log("services subpage");
                if (params.subPageName != '') {
                    console.log('/img/' + params.pageName + '_' + params.subPageName + '_showcase.jpg');
                    blazeObj = {};
                    blazeObj.header = 'servicesHeader';
                    blazeObj.main = params.pageName + '_' + params.subPageName;
                    blazeObj.footer = 'footer';
                    blazeObj.showcaseBg = '/img/' + params.pageName + '_' + params.subPageName + '_showcase.jpg';
                    blazeObj.bodyBg = '/img/services_bg.svg';
                    if (params.subPageName == 'construction_site_materials_testing') {
                        console.log("CONSTRUCTION SITE");
                        blazeObj.pageTitle = 'Construction Site Materials Testing and Observation';
                        blazeObj.bodyBg = 'bg_services_construction_site_materials_testing';
                        return BlazeLayout.render("aboutLayout", blazeObj);
                    } else if (params.subPageName == 'geotechnical_engineering') {
                        blazeObj.pageTitle = 'Geotechnical Engineering';
                        blazeObj.titleCaption = 'At NGE-TFT, we specialize in subsurface exploration, design, and construction in both arctic and sub-arctic regions.';
                    }

                    else if (params.subPageName == 'icc_special_inspection') {
                        console.log("ICC");
                        blazeObj.pageTitle = 'ICC Special Inspection';
                        blazeObj.subLinks = [{
                            title: 'Concrete Placement',
                            link: '/services/icc_special_inspection/concrete'
                        }, {
                            title: 'Concrete Reinforcement',
                            link: '/services/icc_special_inspection/concrete_reinforcement'
                        }, {
                            title: 'Drilled In Concrete Anchors (DICA)',
                            link: '/services/icc_special_inspection/dica'
                        }, {
                            title: 'Fire Proofing Inspection',
                            link: '/services/icc_special_inspection/fireproofing'
                        }, {
                            title: 'High Strength Bolting Inspection',
                            link: '/services/icc_special_inspection/bolting'
                        }, {
                            title: 'Masonry Inspection (CMU-concrete masonry unit)',
                            link: '/services/icc_special_inspection/masonry'
                        }, {
                            title: 'Pre-Stressed Post Tensioning Concrete',
                            link: '/services/icc_special_inspection/pre_post_tensioning'
                        }, {
                            title: 'Welding Inspection',
                            link: '/services/icc_special_inspection/welding'
                        }, {
                            title: 'Structural Wood Framing',
                            link: '/services/icc_special_inspection/structural'
                        }, {
                            title: 'Soils Inspection',
                            link: '/services/icc_special_inspection/soils'
                        }];
                        return BlazeLayout.render("aboutLayout", blazeObj);
                    } else if (params.subPageName == 'lab_materials_testing') {
                        blazeObj.pageTitle = 'Lab Materials Testing';
                        blazeObj.subLinks = [{
                            title: 'Construction Materials Laboratory Testing',
                            link: '/services/lab_materials_testing/construction_materials_testing'
                        }, {
                            title: 'Specialty Geotechnical Laboratory Testing',
                            link: '/services/lab_materials_testing/specialty_lab_testing'
                        }, {
                            title: 'Other Laboratory Service',
                            link: '/services/lab_materials_testing/other'
                        }];
                        blazeObj.indexBlurbRaw = '<h4>Testing Solutions</h4><p>We offer a full range of materials testing solutions, ranging from basic classification analyses (such as material gradations, moisture content determinations, etc.) to more advanced testing services (such as consolidation, thaw consolidation, hydraulic conductivity and triaxial strength testing, etc.). We also have the ability to perform cold regions materials testing, and we maintain a temperature-controlled cold room. A list of the standard tests we perform along with a brief description of the test can be seen by highlighting the links below. However, if you have a unique materials testing problem, we are experts at providing unique, specialized testing solutions to obtain the answer you need. In addition to our Anchorage based laboratory, we have two complete mobile soils testing labs, a mobile asphalt lab (including burn oven) and a concrete break machine, that are ready to ship out at a moment’s notice for your remote project.</p>';

                        return BlazeLayout.render("aboutLayout", blazeObj);
                    }
                    return BlazeLayout.render("aboutLayout", blazeObj);
                } else {
                    console.log("rendering catch all");
                    return BlazeLayout.render("aboutLayout", {
                        header: 'aboutHeader',
                        main: params.pageName,
                        bodyBg: '/img/services_bg.svg',
                        footer: 'footer'
                    });
                }
            } else if (params.pageName == 'people') {
                // probably do nothing ?
                // /img/services_bg.svg
                // support "opportunities"
                if (params.subPageName != '' && params.subPageName == 'opportunities') {
                    return BlazeLayout.render("aboutLayout", {
                        header: 'peopleHeader',
                        pageTitle: "Meet Our Team",
                        main: params.pageName + '_' + params.subPageName,
                        bodyBg: '/img/people_bg.svg',
                        showcaseBg: '/img/' + params.pageName + '_showcase.jpg',
                        footer: 'footer'
                    });
                }
                // handle 404 pages?
            } else if (params.pageName == 'projects') {
                if (params.subPageName == 'geotechnical') {
                    return BlazeLayout.render("projectsLayout", {
                        header: 'projectsHeader',
                        main: params.pageName,
                        subLinks: [{
                            title: "Dams",
                            link: '/projects/geotechnical/dam'
                        }, {
                            title: "Dept. of Transportation",
                            link: '/projects/geotechnical/dpt'
                        }, {
                            title: "Docks",
                            link: '/projects/geotechnical/docks'
                        }, {
                            title: "Foundations",
                            link: '/projects/geotechnical/foundations'
                        }, {
                            title: "Geotech Investigations",
                            link: '/projects/geotechnical/geotech'
                        }, {
                            title: "Mines",
                            link: '/projects/geotechnical/mines'
                        }, {
                            title: "Offshore",
                            link: '/projects/geotechnical/offshore'
                        }, {
                            title: "Pipelines",
                            link: '/projects/geotechnical/pipelines'
                        }],

                        bodyBg: '/img/bg_pattern_1_projects.svg',
                        showcaseBg: false,
                        main: params.pageName + '_' + params.subPageName,
                        //showcaseBg : '/img/' + params.pageName + '_showcase.jpg',
                        footer: 'footer'
                    });
                } else if (params.subPageName == 'special_inspection') {
                    return BlazeLayout.render("projectsLayout", {
                        header: 'projectsHeader ',
                        main: params.pageName,
                        subLinks: [{
                            title: "Education Facilities",
                            link: '/projects/special_inspection/edu'
                        }, {
                            title: "Government",
                            link: '/projects/special_inspection/gov'
                        }, {
                            title: "Medical Facilities",
                            link: '/projects/special_inspection/med'
                        }, {
                            title: "Natural Resources",
                            link: '/projects/special_inspection/nat'
                        }, {
                            title: "Private Sector",
                            link: '/projects/special_inspection/private'
                        }, {
                            title: "Subdivisions",
                            link: '/projects/special_inspection/subd'
                        }, {
                            title: "Transportation",
                            link: '/projects/special_inspection/trans'
                        }],

                        bodyBg: '/img/bg_pattern_1_projects.svg',
                        showcaseBg: false,
                        main: params.pageName + '_' + params.subPageName,
                        //showcaseBg : '/img/' + params.pageName + '_showcase.jpg',
                        footer: 'footer'
                    });
                }
            }
        }
        return false;
    }
});
FlowRouter.route('/:pageName', {
    action: function(params, queryParams) {
        var blazeObj = Context.findOne({urlPath:location.pathname});
        console.log("HERE");
        console.log(blazeObj);
        if(blazeObj){
        //blazeObj.main = params.pageName + '_' + params.subPageName + '__' + params.innerPageName;
            blazeObj.bodyBg = (typeof blazeObj.bodyBg == "undefined" ? '/img/about_bg_b.svg' : blazeObj.bodyBg);
            blazeObj.showcaseBg = '/img/' + params.pageName + '_showcase.jpg';
            if(typeof blazeObj.header == "undefined"){
                blazeObj.header = params.pageName+'Header';
            }
            if(typeof blazeObj.main == "undefined"){
                blazeObj.main = params.pageName;
            }
            if (typeof blazeObj.showcaseBg != "undefined") {
                $("#about_background").backstretch(blazeObj.showcaseBg);
            }
            if(typeof blazeObj.footer == "undefined"){
                blazeObj.footer = 'footer';
            }
            if(typeof blazeObj._layout == "undefined"){
                blazeObj._layout = 'aboutLayout';
            }
            console.log(blazeObj);
            return BlazeLayout.render(blazeObj._layout,blazeObj);
        }else{
            console.log("no blaze obj");
            return false;
        }
    }
});


// hacky workaround for the FlowRouter weirdness.
var backStretch = function(that) {

    $("#about_background").backstretch([that.data.showcaseBg()]);
}

Template.aboutLayout.onRendered(function() {
    $("#about_background").backstretch(this.data.showcaseBg());
});
Template.aboutLayout.onCreated(function() {
    $("#about_background").backstretch(this.data.showcaseBg());
});

Template.about_company_bio.onRendered(function() {
    backStretch(this);
});

Template.about_mission.onRendered(function() {
    backStretch(this);
});

Template.about_code_of_ethics.onRendered(function() {
    backStretch(this);
});

Template.about_news.onRendered(function() {
    backStretch(this);
});

Template.about_pro_affiliations.onRendered(function() {
    backStretch(this);
});
Template.about_client_reviews.onRendered(function() {
    backStretch(this);
});

Template.services_construction_site_materials_testing.onRendered(function() {
    backStretch(this);
});

Template.services_geotechnical_engineering.onRendered(function() {
    backStretch(this);
});

Template.services_geotechnical_engineering__soil_and_bedrock.onRendered(function() {
    backStretch(this);
});

    Template.services_geotechnical_engineering__subsurface_exploration.onRendered(function() {
    console.log(this);
    backStretch(this);
});

Template.people.onRendered(function() {
    backStretch(this);
});

Template.people_opportunities.onRendered(function() {
    backStretch(this);
});