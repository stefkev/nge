
Template.peopleLayout.onCreated(function(){
    console.log('people layout');
    console.log(this.data);
    console.log(this.data.showcaseBg());
     $("#about_background").backstretch([this.data.showcaseBg()]);
});
Template.peopleLayout.onRendered(function(){
    console.log('people layout');
    console.log(this.data);
    console.log(this.data.showcaseBg());
     $("#about_background").backstretch([this.data.showcaseBg()]);
});
Template.people.helpers({
    'teamMembers' : function(){
        return [
            {
                name: "Keith F. Mobley, P.E., G.E.",
                title: "President",
                extraTitle : "Alaska CE 5066, California CE 30683, California GE 2389",
                bioLink : 'kMobely',
                bioImage : 'bio_img_kmobley.jpg'
            },
            {
                name: "Ron J.P. Caron, C.E.T.",
                title: "Vice President, Construction Materials Testing Director",
                bioLink : 'rCaron',
                bioImage : 'bio_img_rcaron.jpg'
            },
            {
                name: "Norman Brent Martin",
                title: "Senior Special Inspector",
                bioLink : 'nbMartin',
                bioImage : 'bio_img_nbmartin.jpg'
            },
            {
                name: "Mark Dean",
                title: "Construction Materials Testing Field Manager",
                bioLink : 'mDean',
                bioImage : 'bio_img_mdean.jpg'
            },
            {
                name: "Andrew F. Fortt, PhD",
                title: "Project Engineer/Laboratory Manager",
                bioLink : 'aFortt',
                bioImage : 'bio_img_afortt.jpg'
            },
            {
                name: "Andrew C. Smith, CPG (AK #663)",
                title: "Senior Geologist",
                bioLink : 'aSmith',
                bioImage : 'bio_img_asmith.jpg'

            },
            {
                name: "Cody J. Kreitel, P.E.",
                title: "Senior Project Engineer",
                extraTitle : "Alaska CE 14064",
                bioLink : 'cKreitel',
                bioImage : 'bio_img_ckreitel.jpg'
            },
            {
                name: "Clinton J. Banzhaf, P.E.",
                title: "Senior Project Engineer",
                extraTitle : "Alaska CE 14856",
                bioLink : 'cBanzhaf',
                bioImage : 'bio_img_cbanzhaf.jpg'
            },
            {
                name: "Xiaoxuan Eva Ge, E.I.T.",
                title: "Project Engineer",
                bioLink : 'xGe',
                bioImage : 'bio_img_xge.jpg'
            },
            {
                name : "Shelley A. McCoy, E.I.T.",
                title: "Project Engineer",
                bioLink:'sMcCoy',
                bioImage : 'bio_img_smccoy.jpg'
            },
            {
                name : "Sean C. Totzke",
                title: "Senior Technical Inspector",
                bioLink:'sTotzke',
                bioImage : 'bio_img_stotzke.jpg',

            },
            {
                name : "Joy Gray",
                title : 'Office Administrator and Human Resources Coordinator',
                bioLink:false,
                bioImage : 'bio_img_jgray.jpg',
                altLink : 'jGray'
            },
            {
                name : "Aimee Goodwin",
                title : 'Geologist Intern',
                bioLink:'aGoodwin',
                bioImage : 'bio_img_agoodwin.jpg'
            }
        ];
    }
});

Template.people_opportunities.helpers({
    'jobOps' : function(){
        return [
            {
                title : "Estimator–Construction",
                location : "Anchorage, AK",
                jobType : "Full Time",
                education : "HS/Some College",
                experience : "Min. 5 years",
                responsibilities : "Projects and controls construction costs by collecting and studying information; controlling construction costs."
            },
            {
                title : "Receptionist",
                location : "Anchorage, AK",
                jobType : "Full Time",
                education : "HS",
                experience : "Min. 2 years",
                responsibilities : "Answers phones in a professional manner and assists with directing persons’ inquiries to the appropriate departments. May also include general office tasks as assigned."
            }
        ]
    }

});