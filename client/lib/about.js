
Template.aboutLayout.onCreated(function(){
     $("#about_background").backstretch([this.data.showcaseBg()]);
});
Template.aboutLayout.helpers({
    'getBackLink' : function(evt,tmpl){
        console.log('wtf');
        console.log(location.pathname);
        var loc = location.pathname.split('/');
        if(loc.length > 1){
            //return second to last element should be good to go!
            // gotta double check specific pages ? 
            loc.pop()
            return location.origin+loc.join('/');
        }
        return false;
    }
});
Template.aboutLayout.events({
    'click .generatedBackLink' : function(){
        console.log("here");
    }
})
Template.about_news.helpers({
    'newsItems' : function(){
        var exampleContent = "This is sample text."  + "Engineering and materials testing are important and learned professions."
                     + "As members of these professions, Engineers and Engineering Technicians are"
                     + "expected to exhibit the highest standard of  honesty  and  integrity." 
                     + "Engineering  and  technical  practices  have  a  direct  and vital impact"
                     + "on the quality of life for all people.  Accordingly, the services provided by Engineers"
                     + " and Engineering Technicians  require  honesty,  impartiality,  fairness,  and  equity, "
                     + " and  must  be  dedicated  to  the protection of the public health, safety, and welfare."
                     + " Engineers and Engineering Technicians must perform  under  a  standard  of  professional"
                     + " behavior  that requires  adherence  to  the  highest principles of ethical conduct. Engineers"
                     + " and  Engineering  Technicians  uphold  and  advance  the integrity,  honor,  and  dignity  of the";

        var first_news = "Keith  F.  Mobley,  P.E.  (President)  and  Norman  Martin  (Senior  ICC  Special  Inspector)  recently attended  the  2016  OTC  Arctic  Technology  Conference  in  St.  John’s,  Newfoundland,  Canada. The  two  hosted  a  booth  which  showcased  NGE-TFT’s  arctic  engineering  and  construction inspection experience and enabled conference attendees a chance to familiarize themselves with NGE-TFT and their services and discuss past, current, and upcoming projects.";
        var second_news = "NGE-TFT would like to congratulate Xiaoxuan “Eva” Ge, P.E. and Shelly McCoy, P.E., our two newest registered Professional Engineers, for completing and successfully passing the Principles and  Practice  of  Engineering  (PE)  exam  in  October  2016. Eva  succeeded in  obtaining  her professional engineering license in the State of California, while Shelley will be registered in the State of Alaska.";
        var third_news = "The Geoprofessional Business Association’s (GBA) Winter Leadership Conference  is an annual meeting for the Board of Directors and for members of GBA’s Councils, Committees, and Task Forces  to  convene  for  a  strategic  planning  and  working  sessions. It is an opportunity  for our volunteers to complete assignments together for the current fiscal year and identify and prioritize actions to focus on  in the  future year. In short, it is a fun couple of days of collaborating where we  all  work together to  help  keep  GBA  moving  forward. Keith  F.  Mobley,  P.E.  (President),  a member of the GBA Business Council, to attend.";
        return [
            {
                headline: "NGE-TFT Attends Annual OTC Arctic Technologies Conference",
                content: first_news,
                datePublished: "October 24-26, 2016",
                image: '/img/artic_tech_conf.jpg'
            },
            {
                headline: "Two Staff EITs Join the Ranks of NGE-TFT’s Registered Professional Engineers",
                content: second_news,
                datePublished: "December 2016",
                image: '/img/NGE_Events_Eva_and_Shelley.jpg'
            },
            {
                headline: "GBA 2017 Winter Leadership Conference",
                content: third_news,
                datePublished: "January 27, 2017",
                image: '/img/gba-logo-1-2.png'
            }
        ]
    }
});

Template.about_pro_affiliations.helpers({
    proAffiliations : function(){
        return [
            {
                headline: "Geoprofessional Business Association (GBA)",
                content: "GBA is a not-for-profit organization whose member firms provide geotechnical, geologic, environmental, construction-materials engineering and testing (Construction Materials Testing), and related geoprofessional services that are essential to the welfare of society and the economy. Value is derived from the wise deployment of geoprofessional expertise where below-ground (“subsurface”), ground-surface, and ground-surface-connected conditions, structures, or formations present challenges and opportunities. GBA exists to help its member Firms and their clients confront risk and optimize performance. ",
                link : 'http://www.geoprofessional.org ',
                linkTitle : 'www.geoprofessional.org',
                image: '/img/about_pro_affiliations_logo_gba.svg'
            },
            {
                headline: "Association of Civil Engineers (ASCE)",
                content: "Founded in 1852, ASCE is the nation’s oldest engineering society. ASCE stands at the forefront of a profession that plans, designs, constructs, and operates society’s economic and social engine – the built environment – while protecting and restoring the natural environment.",
                link : 'http://www.asce.org',
                linkTitle : 'www.asce.org',
                image: '/img/about_pro_affiliations_logo_asce.png'
            },
            {
                headline: "American Council of Engineering Companies (ACEC)",
                content: "ACEC is the voice of America's engineering industry. Council members – numbering more than 5,000 firms representing more than 500,000 employees throughout the country – are engaged in a wide range of engineering works that propel the nation's economy, and enhance and safeguard America's quality of life. These works allow Americans to drink clean water, enjoy a healthy life, take advantage of new technologies, and travel safely and efficiently. The Council's mission is to contribute to America's prosperity and welfare by advancing the business interests of member firms",
                link : 'http://www.acec.org',
                linkTitle : 'www.acec.org',
                image: '/img/about_pro_affiliations_logo_acec.png'
            },
            {
                headline: "Association of Engineering and Environmental Geologists (AEG)",
                content: "AEG is the acknowledged international leader in environmental and engineering geology, and is greatly respected for its stewardship of the profession. AEG offers information on environmental and engineering geology useful to practitioners, scientists, students, and the public. Other geosciences organizations recognize the value of using and sharing AEG's outstanding resources.",
                link : 'http://www.aegweb.org',
                linkTitle : 'www.aegweb.org',
                image: '/img/about_pro_affiliations_logo_aeg.png'
            },
            {
                seperatorHeadline : "Regulatory and Governing Organizations/Agencies: "
            },
            {
                headline: "American Association of State Highway and Transportation Officials (AASHTO)",
                content: "AASHTO is a nonprofit, nonpartisan association representing highway and transportation departments in the 50 states, the District of Columbia, and Puerto Rico. It represents all five transportation modes: air, highways, public transportation, rail, and water. Its primary goal is to foster the development, operation, and maintenance of an integrated national transportation system.  AASHTO is an international leader in setting technical standards for all phases of highway system development. Standards are issued for design, construction of highways and bridges, materials, and many other technical areas.",
                link : 'http://www.transportation.org',
                linkTitle : 'www.transportation.org',
                image: '/img/about_pro_affiliations_logo_aashto.png'
            },
            {
                headline: "American Concrete Institute (ACI)",
                content: "ACI is a leading authority and resource worldwide for the development and distribution of consensus-based standards, technical resources, educational programs, and proven expertise for individuals and organizations involved in concrete design, construction, and materials, who share a commitment to pursuing the best use of concrete.",
                link : 'http://www.concrete.org',
                linkTitle : 'www.concrete.org',
                image: '/img/about_pro_affiliations_logo_aci.png'
            },
            {
                headline: "Western Alliance for Quality Transportation Construction (WAQTC)",
                content: "WAQTC is comprised of representatives of the western states of Alaska, Colorado, Hawaii, Idaho, Montana, New Mexico, Oregon, Texas, Utah, and Washington, and the Western and Central Federal Lands Highway Divisions (WFLHD and CFLHD) of the Federal Highway Administration (FHWA). This organization is dedicated to improving the quality of the transportation products and services that we provide. WAQTC is focused in three main areas: Standardizing test methods (WAQTC, AASHTO, & ASTM), accreditation of the Transportation Technician Qualification Program (TTQP), and working together on national programs of significance including research, training, and technology deployment.",
                link : 'http://www.concrete.org',
                linkTitle : 'www.concrete.org',
                image: '/img/about_pro_affiliations_logo_waqtc.png'
            },
            {
                headline: "International Code Council (ICC)",
                content: "ICC is a member-focused association. It is dedicated to developing model codes and standards used in the design, build and compliance process to construct safe, sustainable, affordable and resilient structures. Most U.S. communities and many global markets choose the International Codes.",
                link : 'http://www.iccsafe.org',
                linkTitle : 'www.iccsafe.org',
                image: '/img/about_pro_affiliations_logo_icc.png'
            },
            {
                headline: "American Society for Testing and Materials (ASTM)",
                content: "ASTM International is a globally recognized leader in the development and delivery of voluntary consensus technical standards for a wide range of materials, products, systems, and services. ASTM standards govern a large majority of the testing methods we employ at NGE-TFT.",
                link : 'http://www.astm.org',
                linkTitle : 'www.astm.org',
                image: '/img/about_pro_affiliations_logo_astm.png'
            },
            {
                headline: "AASHTO Materials Reference Library (AMRL)",
                content: "AMRL is part of the Engineering and Technical Services division of AASHTO (American Association of State Highway and Transportation Officials). The primary vision of AMRL is to be the center for promoting quality and achievement of excellence in construction materials testing.  AMRL accomplishes this by providing services and tools through three major programs: the Laboratory Assessment Program (LAP), the Proficiency Sample Program (PSP), and the AASHTO Accreditation Program (AAP).  Through these activities, AMRL evaluates testing competency, promotes continual improvement, and instills confidence in the laboratories and specifiers that use its programs.",
                link : 'http://www.amrl.net',
                linkTitle : 'www.amrl.net',
                image: '/img/about_pro_affiliations_logo_amrl.png'
            },
            {
                headline: "Cement and Concrete Reference Laboratory (CCRL)",
                content: "CCRL was established in 1929 in an effort to standardize the specifications and methods for testing portland cement. CCRL laboratory inspections were initially limited to laboratories performing physical tests on hydraulic cements. The inspection activity was gradually expanded to include concrete testing and ASTM Committee C-9 on Concrete and Concrete Aggregates The CCRL Laboratory Inspection Program has expanded in scope over the years to include cement, concrete, aggregate, steel reinforcing bars, pozzolan, and masonry materials (mortar and solid units).  CCRL is also responsible for the distribution of proficiency samples for interlaboratory testing.",
                link : 'http://www.ccrl.us',
                linkTitle : 'www.ccrl.us',
                image: '/img/about_pro_affiliations_logo_ccrl.png'
            },
            {
                headline: "Municipality of Anchorage (MOA) Building Safety",
                content: "MOA Building Safety is responsible for guiding safe construction and responsible development for the community through plan review, permitting, and inspections.  MOA Building Safety is also responsible for upholding the MOA’s Special Inspection program which governs the rules and regulations concerning third-party special inspection services and licensing.", 
                subContent : "Find more information about MOA Building Safety at: http://www.muni.org/Departments/OCPD/development/BSD/Pages/default.aspx <br/> Find more information about the MOA Special Inspection Program at: http://www.muni.org/Departments/OCPD/development/BSD/BSDDocuments/ SpecialInspectionGuidelines.pdf",
                //link : 'www.ccrl.us',
                //linkTitle : 'www.ccrl.us',
                image: '/img/about_pro_affiliations_logo_moa.png'
            },
            {
                headline: "Municipality of Anchorage (MOA) Geotechnical Advisory Commission (GAC)",
                content: "The MOA GAC acts in an advisory capacity to the Assembly, Mayor, municipal departments, Planning and Zoning Commission, Platting Board, Building Board, Building Safety, and the professional design community by providing professional advice on issues relating to natural hazards risk mitigation.", 
                link : 'http://www.muni.org/Departments/OCPD/Planning/Pages/Boards.aspx#GAC ',
                linkTitle : 'www.muni.org/Departments/OCPD/Planning/Pages/Boards.aspx#GAC ',
                image: '/img/about_pro_affiliations_logo_moa.png'
            },
            {
                headline: "US Army Corps of Engineers (USACOE) Materials Testing Center (MTC)",
                content: "The USACOE MTC supplies inspection services of commercial laboratories for quality assurance and quality control testing. Located at the ERDC Waterways Experiment Station site in Vicksburg, MS, the MTC is the only agency authorized to validate commercial laboratories to work for the Corps of Engineers.", 
                link : 'http://www.erdc.usace.army.mil/Media/FactSheets/FactSheetArticleView/tabid/9254/Article/476661/materials-testing-center.aspx',
                linkTitle : 'www.erdc.usace.army.mil/Media/FactSheets/FactSheetArticleView/tabid/9254/Article/476661/materials-testing-center.aspx',
                image: '/img/about_pro_affiliations_logo_mtc.svg'
            },
            {
                headline: "American Welding Association (AWS)",
                content: "Advancing the science, technology, and application of welding and allied joining and cutting processes worldwide: that’s our mission and it’s why we exist. Whether you’re here to explore membership, certification, advanced training, updated standards, conferences, professional collaborations or the many exciting career opportunities in welding today – we are here to support you. Count on AWS for the leading-edge industry knowledge, resources and tools you need to achieve even greater business and career success",
                link : 'http://www.aws.org',
                linkTitle : 'www.aws.org',
                image: '/img/about_pro_affiliations_logo_mtc.png'
            }
        ];
    }

});
  var client_reviews_index =
    [
        'Alaska Department of Natural Resrouces',
        'Duane Miller & Associates',
        'White Raven Development',
        'GMC Contracting Inc.',
        'The Line Works',
        'KC Alaska, Inc.',
        'Kiewit Construction',
        'Lounsbury & Associates, Inc.',
        'H4M Corporation',
        'CH2M Hill',
        'Alaska Botanical Garden',
        'UAA Community & Technical College'
    ];
var sampleClientReviews = function(){
   

}



Template.about_client_reviews.helpers({
    sampleClientIndex : function(){
        return client_reviews_index;
    },
    sampleClientReviews : function(){
        sampleText = 'This is sample text. Engineering and materials testing are important and learned professions.  As members of these professions, Engineers and Engineering Technicians are expected to exhibit the highest standard of  honesty  and  integrity.    Engineering  and  technical  practices  have  a  direct  and vital impact  on the quality of life for all people.  Accordingly, the services provided by Engineers and Engineering Technicians  require  honesty,  impartiality,  fairness,  and  equity,  and  must  be  dedicated  to  the protection of the public health, safety, and welfare.  Engineers and Engineering Technicians must perform  under  a  standard  of  professional  behavior  that requires  adherence  to  the  highest principles of ethical conduct.Engineers  and  Engineering  Technicians  uphold  and  advance  the integrity,  honor,  and  dignity  of the';
        // r = ['about_c_img_1','about_c_img_2','about_c_img_3'];
        r = [
            {
                client: 'Alaska Department of Natural Resources',
                date: 'August 6, 2002',
                link: '/pdf/AK_Dept_of_Natural_Resources.pdf',
                content:'Terra Firma Inc. (TFI) and specifically Ron Caron and Tamara Selmer have worked with the Department of Natural Resources, Design and Construction Section on several projects as testing and quality control consultants over the past three to four years. Work performed by Terra Firma Inc. included material testing for Bird Point to Girdwood Pathway, Bird Point Scenic Overlook, Bird Creek Pedestrian Facilities, Hatcher Pass Scenic Overlook and Trailheads, and the Anchorage Trail Rehabilitation projects. These projects total approximately $7 million dollars in construction.'
            },
            {
                client: 'Duane Miller & Associates',
                date: 'August 24, 2002',
                link: '/pdf/Duane_Miller_&_Associates.pdf',
                content: 'Terra Firma has provided geotechnical testing for Duane Miller & Associates since 1998 when Ron Caron and Tamara Selmer established their business. Before that, Ron had been the lead person for testing services for many of my projects back to 1990. The Terra Firma laboratory has provided soils testing for us on a timely and efficient basis with a high level of quality. They have readily adapted our requirements for digital reporting of test data.'
            },
            {
                client: 'White Raven Development',
                date: 'August 26, 2002',
                link: '/pdf/White_Raven.pdf',
                content: 'It has been a pleasure having you as a part of our Land Development Team. Your professionalism and knowledge of soils has made Terra Firma a name we count on. As we close out 2002, we continue planning for 2003 we look forward to your continued participation with our future development.'
            },
            {
                client: 'GMC Contracting Inc.',
                date: 'August 27, 2002',
                link: '/pdf/GMC.pdf',
                content: 'At the request of Terra Firma, Inc. I am writing this letter of recommendation for Ron and Tamara. It is my understanding they are trying to qualify as a soils testing agency for some future projects. I cannot think of a better firm.I have known both Ron and Tamara since their days of working for EBA Engineering. When I first met Ron while at EBA, I quickly became impressed with his knowledge in the field and his skills at documentation. The first year they began Terra Firma, a problem on a GMC project came up.'
            },
            {
                client: 'The Line Works',
                date: 'August 27, 2002',
                link: '/pdf/Line_Works.pdf',
                content: 'During the past four years, I have had the opportunity to work with Terra Firma on many of my projects, to mention a few, Kincaid Estates North, Brookside Knoll Subdivision and Spruce Meadows Subdivision. The professionalism that both Ron Caron and Tamara Selmer have displayed has been above reproach. They have a very experienced staff who are not only knowledgeable with soils, soils testing and equipment, they are dependable and friendly and a pleasure to have on the project site.'
            },
            {
                client: 'KC Alaska, Inc.',
                date: 'September 2, 2002',
                link: '/pdf/KC_Alaska.pdf',
                content: 'Over the past few years, we have used Ron Caron and Tamara Selmer of Terra Firma, Inc. for material testing on our jobs, primarily last year on the Finger Lake Boat Ramp and Susitna Landing Boat Ramp. They have also been doing our testing and soil reports at Denali Elementary School and the Chugiak High School. Our experience with Terra Firma has been excellent.'
            },
            {
                client: 'Kiewit Construction',
                date: 'September 3, 2002',
                link: '/pdf/Kiewit.pdf',
                content: 'We have utilized Terra Firma Inc. for both materials testing and Contractor Quality Control services. We have never received any but the best service from Terra Firma. Ron Caron and Tamara Selmer make themselves available whenever necessary and truly understand a contractors needs in the construction industry.'
            },
            {
                client: 'Lounsbury & Associates, Inc.',
                date: 'September 6, 2002',
                link: '/pdf/Lounsbury.pdf',
                content: 'Lounsbury and Associates has worked with Terra Firma Inc. (TFI) two different times in the past. I know them to be thorough and professional. In the first instance they were the testing lab for an underground utilities project. They were always able to respond on relatively short notice as the contractor completed portions of the work. This proved particularly important in the testing trench backfill.'
            },
            {
                client : 'H4M Corporation',
                date: 'September 7, 2002',
                link: '/pdf/H4M.pdf',
                content: 'Terra Firma is well-established Alaskan organization that provides timely professional materials testing to the Construction Family, including agencies, engineers and contractors. In many ways they remind me of our early days developing Alaska Testlab, 1957 to 1973, for similar purposes. They show great energy and attention to fundamentals.'
            },
            {
                client: 'CH2M Hill',
                date: 'September 8, 2002',
                link: '/pdf/CH2M_Hill.pdf',
                content: 'I have retained Terra Firma through Ron Caron and Tamara Selmer for field and laboratory soils testing on numerous projects since their firm was founded in 1998. They have consistently provided excellent service in a timely manner with no quality control problems. I have worked with Ron and Tamara on Alaska projects since the early 1980’s, and have full confidence in their capabilities and knowledge regarding laboratory testing and construction control work.'
            },
            {
                client: 'Alaska Botanical Garden',
                date: 'December 31, 2014',
                link: '/pdf/AK_Botanical_Garden.pdf',
                content: 'Thank you for NGE-TFT’s generous in kind donation of the geotechnical explorations and report valued at $7,178.00 to the Alaska Botanical Garden on 10/23/2014. Your support is greatly appreciated! Generous donations such as this make it possible for ABG to continue to grow and serve the community through its mission of education, preservation, recreation and research.'
            },
            {
                client: 'UAA Community & Technical College',
                date: 'March 10, 2016',
                link: '/pdf/UAA_Comm_&_Tech_College.pdf',
                content: 'On behalf of the faculty, staff and students of the Community & Technical College of the University of Alaska Anchorage, thank you for your recent gift. Your generous contribution has been allocated to the Excellence in Construction Management Fund and will help support and build our college’s career and technical education programs and services.'
            }
        ]
        // client_reviews_index.filter(function(company,i){
        //     console.log(company);
        //     var images = [''];
        //     console.log(i  % 3);
        //     var index= i% 3 + 0
        //     r.push({
        //         client : company,
        //         date : new Date(),
        //         link :'#',
        //         content : sampleText
        //         //image : '/img/' + r[index] + '.jpg'
        //     });
        // });
        return r;
    }
});
