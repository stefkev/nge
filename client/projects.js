Template.projectsLayout.onCreated(function(){
	console.log('projectsLayout rendered');
	if(typeof this.data != "undefined" && typeof this.data.content != "undefined"){
		var content = this.data.content();
		if(typeof content != "undefined"){
			if(content.length > 1){
				Session.set("hasMoreThanOne",true);
			}
			Session.set("activeSlide",content[0].title);
		}
		else {
			Session.set("activeSlide",undefined);
		}
	}
	
});


Template.projectsLayout.onRendered(function(){
	console.log('projectsLayout rendered');
	
	if(typeof this.data != "undefined" && typeof this.data.content != "undefined"){
		var content = this.data.content();
		if(typeof content != "undefined"){
			if(content.length > 1){
				Session.set("hasMoreThanOne",true);
			}
			Session.set("activeSlide",content[0].title);
		}
		else {
			Session.set("activeSlide",undefined);
		}
	}
	
});


Template.projectsLayout.helpers({
	'isActive' : function(){
		if(Session.equals("activeSlide",undefined)){
			Session.set("activeSlide",this.title);
		}
		var content = this;
		if(Session.equals('activeSlide',content.title)){
			return true;
		}else{
			return false;
		}
	},
	'hasMoreThanOne' : function(){
		return Session.get("hasMoreThanOne");
	},
	getProjectsBackLink : function(){
		// go to projects?
		// this is home : 
		console.log('hello');
		if(location.pathname == '/projects/geotechnical/dam'){
			return location.origin ;
		}else{
			var path = location.pathname.split('/');
			return location.origin  + '/projects/geotechnical/dam';
		}
		return '/';
	}	
});

Template.projectsLayout.events({
	'click .next' : function(){
		var content = Template.parentData(-1).content();
		if(typeof this.title != "undefined"){
			// figure out the next set
			var slidesLength = content.length;
			var currentIndex = -1;
			content.filter(function(a,i){
				if(Session.equals('activeSlide',a.title)){
					console.log("updating current index " + a.title);
					currentIndex = i;
				}
			});
			// if i < length + 1 then increment otherwise set to zero
			if(currentIndex < (slidesLength-1)){
				// add one!
				Session.set('activeSlide',content[currentIndex+1].title);
			}else{
				console.log("TOO MUCH");
				Session.set('activeSlide',content[0].title);
			}
		}
	},
	'click .prev' : function(){
		//console.log(this);
		var content = Template.parentData(-1).content();
		if(typeof this.title != "undefined"){
			// figure out the next set
			var slidesLength = content.length;
			var currentIndex = -1;
			content.filter(function(a,i){
				if(Session.equals('activeSlide',a.title)){
					console.log("updating current index " + a.title);
					currentIndex = i;
				}
			});
			// if i < length + 1 then increment otherwise set to zero
			if(currentIndex >= (slidesLength-1)){
				// add one!
				Session.set('activeSlide',content[currentIndex-1].title);
			}else{
				//console.log("TOO LITTLE");
				// send to end!
				if(currentIndex == 1){
					Session.set('activeSlide',content[0].title);
				}else{
					Session.set('activeSlide',content[content.length-1].title);
				}
				return true;
			}
		}

	},
	'click li a' : function(){
		console.log(this);
		var content = Template.parentData(-1).content();
		Session.set("activeSlide",content[0].title);
		if(typeof content != "undefined"){
			if(content.length > 1){
				Session.set("hasMoreThanOne",true);
			}
			Session.set("activeSlide",content[0].title);
		}
		else {
			Session.set("activeSlide",undefined);
		}
		// such hack. forces page to reload otherwise flow router 
		// doesn't update everything. Replace with ui framework (react/angular)
		location.reload();
	}
})