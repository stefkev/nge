Template.simpleUpload.onCreated(function(){

  var self = this;
  this.autorun(function(){
    self.subscribe("users");
    self.subscribe("documents");
  });
});

Template.simpleUpload.helpers({
  'getDocs': function(){
    return Documents.find();
  }

});

Template.simpleUploadOwner.helpers({
  'getUsers': function(){
    // mark the option that is selected as the current owner;
    var u = Meteor.users.find().fetch();
    var r = [];
    var owner = this.owner;
    u.filter(function(o){
      if(o._id == owner){
        o.selected="selected";
      }
      r.push(o);
    })
    return r;
//    return Meteor.users.find().fetch();
    //return ;
  }

});

Template.simpleUploadOwner.events({
  'change .fileOwner' : function(evt,tmpl){
    var owner = tmpl.find(".fileOwner");
    Meteor.call("setDocumentOwner",this._id,owner.value)
  }
})
Template.simpleUpload.events({
   'click .removeFile' : function(){
    if(Meteor.userId() && Roles.userHasRole(Meteor.userId(),'admin')){
      return Documents.remove({_id : this._id});
    }else{
      alert("You do not have permission to remove this file");
    }
    return false;
   },
   'change .myFileInput': function(event, template) {
    uId = Meteor.userId();

      FS.Utility.eachFile(event, function(file) {
        //file.uid = uId;
        file.metadata = {
            imageOwner:Meteor.userId()
        }
        //console.log(file);
        Documents.insert(file, function (err, fileObj) {
          if (err){
             // handle error
          } else {
            console.log("Uploaded file");
             // handle success depending what you need to do
            //var userId = Meteor.userId();
            //var imagesURL = {
            //  'profile.image': '/cfs/files/images/' + fileObj._id
            //};
            //Meteor.users.update(userId, {$set: imagesURL});
          }
        });
     });
   },
    'click .emailLink': function(evt,tmpl){
      console.log(this);
      Meteor.call('sendEmailDocumentLink',this.owner,this._id,function(e,r){
        console.log(e);
        console.log(r);
      });
    }
 });