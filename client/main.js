/*
 *	Barebones setup clientside
 *
 */
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import './main.html';
// prefer iron router syntax

Template.slideshow_head.onCreated(function() {
    $('.cycle-slideshow').cycle();
});
Template.slideShow.onRendered(function(){
	 $('.cycle-slideshow').cycle();
	var $pagerItems = $('#pager > span');
	$(".cycle-slideshow li").each(function(i) {
		divclass = $(this).data('background');
		divtitle = $(this).data('title');
		divlink = $(this).data('link');
		$pagerItems.eq(i).append('<a href="' + divlink + '">' + divtitle + '</a>');
		$pagerItems.eq(i).addClass(divclass);
	});
	
});

/*
Template.collTest.onCreated(function(){
	console.log('collTest created');
	if(Meteor.userId){
		Meteor.subscribe('users');
	}
});

Template.collTest.events({
	'click td' : function(evt,tmpl){
		Session.set('currentDoc',this);
	}
});
Template.collTest.helpers({
	getOwner : function(evt,tmpl){
		console.log(this);
		var u =Meteor.users.findOne({_id : this.metadata._auth.owner});
		console.log(u);
		return u.profile.name;
	},
	getCurrentSelectedDoc : function(evt,tmpl){
		if(!Session.equals("currentDoc",undefined)){
			return Session.get("currentDoc").filename;
		}else{
			return false;
		}
	}
	}
);
*/